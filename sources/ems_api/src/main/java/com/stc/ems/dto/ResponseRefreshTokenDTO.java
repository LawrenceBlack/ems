package com.stc.ems.dto;

public class ResponseRefreshTokenDTO {
    private int statusCode;
    private String token;
    private String error;

    public ResponseRefreshTokenDTO() {
    }

    public ResponseRefreshTokenDTO(int statusCode, String token, String error) {
        this.statusCode = statusCode;
        this.token = token;
        this.error = error;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
