package com.stc.ems.Util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;

public class FileUtil {

    public static void initFolderIfnotExit(String pathFolder){
        try {
            Path folderPath= Paths.get(pathFolder);

            if(Files.notExists(folderPath))
            {
                Files.createDirectories(folderPath);
            }
        }catch (IOException e){
            throw new RuntimeException("Could not initialize storage !");
        }
    }
}
