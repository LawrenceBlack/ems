package com.stc.ems.entity.QualityGoals;

import com.stc.ems.entity.Unit;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document
//Mục tiêu chất lượng toàn trường
public class QualityGoals {

    @Id
    private String id;

    @DBRef
    private List<Unit> listUnit;
    private String year;
    private List<TopicParent> listTopicParent;

    public QualityGoals() {
    }

    public QualityGoals(String year) {
        this.year = year;
        this.listUnit=new ArrayList<>();
        this.listTopicParent=new ArrayList<>();
    }

    public QualityGoals(List<Unit> listUnit, String year) {
        this.listUnit = listUnit;
        this.year = year;
        this.listTopicParent=new ArrayList<>();
    }

    public QualityGoals(List<Unit> listUnit, String year, List<TopicParent> listTopicParent) {
        this.listUnit = listUnit;
        this.year = year;
        this.listTopicParent = listTopicParent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Unit> getListUnit() {
        return listUnit;
    }

    public void setListUnit(List<Unit> listUnit) {
        this.listUnit = listUnit;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public List<TopicParent> getListTopicParent() {
        return listTopicParent;
    }

    public void setListTopicParent(List<TopicParent> listTopicParent) {
        this.listTopicParent = listTopicParent;
    }
}
