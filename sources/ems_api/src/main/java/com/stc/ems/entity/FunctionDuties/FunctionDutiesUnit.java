package com.stc.ems.entity.FunctionDuties;

import com.stc.ems.entity.Unit;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

//Chức năng nhiệm vụ đơn vị

@Document
public class FunctionDutiesUnit {

    @Id
    private String id;
    private String year;
    @DBRef
    private Unit unit;
    private List<ParentWorks> parentWorks;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public List<ParentWorks> getParentWorks() {
        return parentWorks;
    }

    public void setParentWorks(List<ParentWorks> parentWorks) {
        this.parentWorks = parentWorks;
    }
}
