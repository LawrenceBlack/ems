package com.stc.ems.controller;

import com.stc.ems.services.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/evidences")
public class EvidenceController {

    @Autowired
    private StorageService storageService;

    @PostMapping(headers =  "content-type=multipart/*",produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file){
        try {
            storageService.store(file);
            return new ResponseEntity<>("Success upload file "+file.getOriginalFilename(),HttpStatus.OK);
        }
        catch (Exception ex){
            return new ResponseEntity<String>("Fail to upload file "+file.getOriginalFilename(),HttpStatus.EXPECTATION_FAILED);
        }

    }
}
