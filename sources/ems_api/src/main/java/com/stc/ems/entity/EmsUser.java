package com.stc.ems.entity;

        import org.springframework.data.annotation.Id;
        import org.springframework.data.mongodb.core.mapping.DBRef;
        import org.springframework.data.mongodb.core.mapping.Document;

        import java.util.List;

@Document
public class EmsUser {
    @Id
    private String id;
    private String userName;
    private boolean enabled;
    private List<UnitRoles> unitRoles;

    public EmsUser() {
    }

    public EmsUser(String userName, boolean enabled, List<UnitRoles> unitRoles) {
        this.userName = userName;
        this.enabled = enabled;
        this.unitRoles = unitRoles;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<UnitRoles> getUnitRoles() {
        return unitRoles;
    }

    public void setUnitRoles(List<UnitRoles> unitRoles) {
        this.unitRoles = unitRoles;
    }
}
