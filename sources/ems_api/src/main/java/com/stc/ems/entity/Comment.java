package com.stc.ems.entity;

import java.util.Date;

//Bình luận
public class Comment {
    private String location; //Use to identity location of Comment
    private String content;
    private String userComment;
    private Date timeComent;
    private Date timeLastEdit;
    private boolean owner;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUserComment() {
        return userComment;
    }

    public void setUserComment(String userComment) {
        this.userComment = userComment;
    }

    public Date getTimeComent() {
        return timeComent;
    }

    public void setTimeComent(Date timeComent) {
        this.timeComent = timeComent;
    }

    public Date getTimeLastEdit() {
        return timeLastEdit;
    }

    public void setTimeLastEdit(Date timeLastEdit) {
        this.timeLastEdit = timeLastEdit;
    }

    public boolean isOwner() {
        return owner;
    }

    public void setOwner(boolean owner) {
        this.owner = owner;
    }
}
