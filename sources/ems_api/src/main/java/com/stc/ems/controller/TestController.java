package com.stc.ems.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Array;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api")
public class TestController {
    @RequestMapping("/users")
    public String  user(Principal principal) {
        return principal.getName();
    }

    @PreAuthorize("hasRole('write')")
    public String write(){
        return "write";
    }

    @PostMapping(value = "/test")
    public ResponseEntity<?> addMangCongViec(@RequestBody List<String> mangCongViecs) {
        List<?> mangcongviec = mangCongViecs.stream().map((o)-> {
            MangCV cv = new MangCV();
            cv.setName(o);
            return cv;
        }).collect(Collectors.toList());
        return new ResponseEntity<>(mangcongviec,HttpStatus.OK);
    }
}


class MangCV {
    public String _id;
    public String name;

    public  MangCV() {

    }
    public MangCV(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

