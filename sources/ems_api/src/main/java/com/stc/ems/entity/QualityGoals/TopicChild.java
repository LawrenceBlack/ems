package com.stc.ems.entity.QualityGoals;

import com.stc.ems.Util.StringUtil;
import com.stc.ems.entity.Comment;

import java.util.ArrayList;
import java.util.List;

//Still working here
//Đề mục con
public class TopicChild {
    private String id;
    private int stt;
    private String detailGoal;
    private String kpis;
    private List<ImplementationPlan> listImplementationPlan;// List kế hoạch thực hiện
    private List<Comment> listComment;

    public TopicChild() {
    }

    public TopicChild(int stt, String detailGoal, String kpis) {
        this.stt = stt;
        this.detailGoal = detailGoal;
        this.kpis = kpis;
        this.listImplementationPlan = new ArrayList<>();
        this.listComment = new ArrayList<>();
    }

    public TopicChild(int stt, String detailGoal, String kpis, List<ImplementationPlan> listImplementationPlan) {
        this.stt = stt;
        this.detailGoal = detailGoal;
        this.kpis = kpis;
        this.listImplementationPlan = listImplementationPlan;
        this.listComment = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getStt() {
        return stt;
    }

    public void setStt(int stt) {
        this.stt = stt;
    }

    public String getDetailGoal() {
        return detailGoal;
    }

    public void setDetailGoal(String detailGoal) {
        this.detailGoal = detailGoal;
    }

    public String getKpis() {
        return kpis;
    }

    public void setKpis(String kpis) {
        this.kpis = kpis;
    }

    public List<ImplementationPlan> getListImplementationPlan() {
        return listImplementationPlan;
    }

    public void setListImplementationPlan(List<ImplementationPlan> listImplementationPlan) {
        this.listImplementationPlan = listImplementationPlan;
    }

    public void addNewImplementationPlan(ImplementationPlan implementationPlan) {
        this.listImplementationPlan.add(implementationPlan);
    }

    public void removeImplementationPlan(ImplementationPlan implementationPlan) {
        this.listImplementationPlan.remove(implementationPlan);
    }

    public void removeImplementationPlan(String id) {
        this.listImplementationPlan.removeIf(s -> s.getId().equals(id));
    }

    public List<Comment> getListComment() {
        return listComment;
    }

    public void setListComment(List<Comment> listComment) {
        this.listComment = listComment;
    }

    public void addNewComment(Comment comment) {
        this.listComment.add(comment);
    }

    public void removeComment(Comment comment) {
        this.listComment.remove(comment);
    }

    public void removeComment(String location) {
        this.listComment.removeIf(s -> s.getLocation().equals(location));
    }
}
