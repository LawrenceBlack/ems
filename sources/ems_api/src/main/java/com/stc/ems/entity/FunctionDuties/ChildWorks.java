package com.stc.ems.entity.FunctionDuties;

import java.util.Date;
import java.util.List;

public class ChildWorks {
    private String id;
    private String nameChildWork;
    private ISOProcedure isoProcedure;
    private String detailChildWork;
    private Storage storage;
    private String userCreate;
    private Date dateCreate;
    private String lastUserEdit;
    private Date lastDateEdit;
    private List<ChangeLog> changeLogs;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNameChildWork() {
        return nameChildWork;
    }

    public void setNameChildWork(String nameChildWork) {
        this.nameChildWork = nameChildWork;
    }

    public ISOProcedure getIsoProcedure() {
        return isoProcedure;
    }

    public void setIsoProcedure(ISOProcedure isoProcedure) {
        this.isoProcedure = isoProcedure;
    }

    public String getDetailChildWork() {
        return detailChildWork;
    }

    public void setDetailChildWork(String detailChildWork) {
        this.detailChildWork = detailChildWork;
    }

    public Storage getStorage() {
        return storage;
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getLastUserEdit() {
        return lastUserEdit;
    }

    public void setLastUserEdit(String lastUserEdit) {
        this.lastUserEdit = lastUserEdit;
    }

    public Date getLastDateEdit() {
        return lastDateEdit;
    }

    public void setLastDateEdit(Date lastDateEdit) {
        this.lastDateEdit = lastDateEdit;
    }

    public List<ChangeLog> getChangeLogs() {
        return changeLogs;
    }

    public void setChangeLogs(List<ChangeLog> changeLogs) {
        this.changeLogs = changeLogs;
    }
}
