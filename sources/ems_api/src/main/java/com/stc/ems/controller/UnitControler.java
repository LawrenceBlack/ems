package com.stc.ems.controller;

import com.stc.ems.dto.PagingDTO;
import com.stc.ems.entity.Unit;
import com.stc.ems.repository.UnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/units")
public class UnitControler {

    @Autowired
    private UnitRepository unitRepository;

    @GetMapping
    public ResponseEntity<List<Unit>> getAllUnit(){
        return new ResponseEntity<List<Unit>>(unitRepository.findAll(), HttpStatus.OK);
    }
    @PostMapping(value = "/listId", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Unit>> getUnitByListId(@RequestBody List<String> unitsId){
        List<Unit> units= unitsId.stream()
                .map(o-> unitRepository.findById(o).orElse(null))
                .filter(unit -> unit !=null)
                .collect(Collectors.toList());
        return new ResponseEntity<>(units,HttpStatus.OK);
    }
    @GetMapping("/paging")
    public ResponseEntity<Page<Unit>> getAllUnit(@RequestBody PagingDTO pagingDTO){
        return new ResponseEntity<Page<Unit>>(unitRepository.findAll(pagingDTO.getPageable()),HttpStatus.OK);
    }
}
