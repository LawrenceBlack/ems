package com.stc.ems.entity;

import com.stc.ems.entity.Comment;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
//Minh chứng
public class Evidence {
    @Id
    private String id;
    private String urlFile;
    private String urlFolder;
    private String type;
    private List<Comment> listComment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrlFile() {
        return urlFile;
    }

    public void setUrlFile(String urlFile) {
        this.urlFile = urlFile;
    }

    public String getUrlFolder() {
        return urlFolder;
    }

    public void setUrlFolder(String urlFolder) {
        this.urlFolder = urlFolder;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Comment> getListComment() {
        return listComment;
    }

    public void setListComment(List<Comment> listComment) {
        this.listComment = listComment;
    }

    public void addNewComment(Comment comment) {
        this.listComment.add(comment);
    }

    public void removeComment(Comment comment) {
        this.listComment.remove(comment);
    }

    public void removeComment(String location) {
        this.listComment.removeIf(s -> s.getLocation().equals(location));
    }
}
