package com.stc.ems.controller;


import com.stc.ems.entity.QualityGoals.QualityGoals;
import com.stc.ems.repository.QualityGoalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

//Mục tiêu chất lượng của trường
@RestController
@RequestMapping("/qualitygoal")
public class QualityGoalController {

    @Autowired
    private QualityGoalRepository qualityGoalRepository;

    @GetMapping("/{year}")
    public ResponseEntity<QualityGoals> getQualityGoalByYear(@RequestParam String year){
        Optional<QualityGoals> optionalQualityGoals= qualityGoalRepository.findFirstByYear(year);
        if(optionalQualityGoals.isPresent())
            return new ResponseEntity<>(optionalQualityGoals.get(),HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @PostMapping
    public ResponseEntity<?> addNewQualityGoal(QualityGoals qualityGoals){
        try {
            qualityGoalRepository.save(qualityGoals);
            return new ResponseEntity<>("Insert Success",HttpStatus.OK);
        }
        catch (Exception ex){
            return new ResponseEntity<>("Have error "+ ex.toString(),HttpStatus.EXPECTATION_FAILED);
        }
    }
    @PutMapping
    public ResponseEntity<?> updateQualityGoal(QualityGoals qualityGoals){
        try {
            QualityGoals goals = qualityGoalRepository.findById(qualityGoals.getId()).get();
            goals = qualityGoals;
            qualityGoalRepository.save(goals);
            return new ResponseEntity<>(true, HttpStatus.OK);
        }
        catch (Exception ex){
            return new ResponseEntity<>("Have error when update with item id: "+qualityGoals.getId(),HttpStatus.EXPECTATION_FAILED);
        }
    }
}
