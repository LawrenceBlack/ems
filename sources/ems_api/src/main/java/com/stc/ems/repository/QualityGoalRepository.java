package com.stc.ems.repository;

import com.stc.ems.entity.QualityGoals.QualityGoals;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface QualityGoalRepository extends MongoRepository<QualityGoals,String> {
    Optional<QualityGoals> findFirstByYear(String year);
}
