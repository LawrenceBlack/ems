package com.stc.ems;

import com.stc.ems.entity.*;
import com.stc.ems.entity.QualityGoals.QualityGoals;
import com.stc.ems.entity.QualityGoals.QualityGoalsUnit;
import com.stc.ems.entity.QualityGoals.TopicParent;
import com.stc.ems.repository.*;
import com.stc.ems.services.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@SpringBootApplication
public class EmsApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(EmsApplication.class, args);
    }

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UnitRepository unitRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public void run(String... args) throws Exception {

//        Unit khoaCNTT = unitRepository.findById("5b1629053861701cc417ee64").get();
//        Unit khoaCKM = unitRepository.findById("5b1629053861701cc417ee63").get();
//
//        Role roleRead = roleRepository.findById("5b1629053861701cc417ee67").get();
//        Role roleWrite = roleRepository.findById("5b1629053861701cc417ee68").get();
//
//        Role roleDownload = roleRepository.findById("5b1629053861701cc417ee66").get();
//        Role roleUpload = roleRepository.findById("5b1629053861701cc417ee65").get();
//
//        Role roleUser = roleRepository.findById("5b178dd8f622d44f741f28d3").get();
//        Role roleAdmin = roleRepository.findById("5b178e31f622d44f741f28d4").get();
//        Role roleSuperAdmin = roleRepository.findById("5b178e6ef622d44f741f28d5").get();
//
//        List<Role> khoaCKMRoles = new ArrayList<>();
//        khoaCKMRoles.add(roleRead);
//        khoaCKMRoles.add(roleWrite);
//        khoaCKMRoles.add(roleAdmin);
//
//        List<Role> khoaCNTTRoles = new ArrayList<>();
//        khoaCNTTRoles.add(roleDownload);
//        khoaCNTTRoles.add(roleUpload);
//        khoaCNTTRoles.add(roleUser);
//
//        UnitRoles unitRolesCKM = new UnitRoles(khoaCKM, khoaCKMRoles);
//        UnitRoles unitRolesCNTT = new UnitRoles(khoaCNTT, khoaCNTTRoles);
//
//        List<UnitRoles> roles = new ArrayList<>();
//        roles.add(unitRolesCKM);
//        roles.add(unitRolesCNTT);
//
//        EmsUser ems_user = new EmsUser("thanhan2014.nguyen@gmail.com", true, roles);
//        userRepository.save(ems_user);
//        EmsUser ems_user_2 = new EmsUser("kieuquanh94@gmail.com", true, roles);
//        userRepository.save(ems_user_2);

    }
}
