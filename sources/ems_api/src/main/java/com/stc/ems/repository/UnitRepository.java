package com.stc.ems.repository;

import com.stc.ems.entity.Unit;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UnitRepository extends MongoRepository<Unit,String> {

}
