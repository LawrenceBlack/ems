package com.stc.ems.controller;

import com.stc.ems.entity.Semester;
import com.stc.ems.repository.SemesterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/semester")
public class SemesterController {

    @Autowired
    private SemesterRepository semesterRepository;


    @GetMapping
    public ResponseEntity<List<Semester>> getAllSemester(){
        return new ResponseEntity<List<Semester>>(semesterRepository.findAll(),HttpStatus.OK);
    }
}
