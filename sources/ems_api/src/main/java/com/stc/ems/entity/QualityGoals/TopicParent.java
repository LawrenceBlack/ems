package com.stc.ems.entity.QualityGoals;

import com.stc.ems.Util.StringUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

//Đề mục cha
public class TopicParent {
    private String id;
    private String stt; //Roman number
    private String nameTopic;
    private List<TopicChild> topicChildren;

    public TopicParent() {
    }

    public TopicParent(int stt, String nameTopic) {
        this.id = UUID.randomUUID().toString();
        this.stt = StringUtil.convertNumberToRomanString(stt);
        this.nameTopic = nameTopic;
        this.topicChildren = new ArrayList<>();
    }

    public TopicParent(int stt, String nameTopic, List<TopicChild> topicChildren) {
        this.id = UUID.randomUUID().toString();
        this.stt = StringUtil.convertNumberToRomanString(stt);
        this.nameTopic = nameTopic;
        this.topicChildren = topicChildren;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStt() {
        return stt;
    }

    public void setStt(String stt) {
        this.stt = stt;
    }

    public String getNameTopic() {
        return nameTopic;
    }

    public void setNameTopic(String nameTopic) {
        this.nameTopic = nameTopic;
    }

    public List<TopicChild> getTopicChildren() {
        return topicChildren;
    }

    public void setTopicChildren(List<TopicChild> topicChildren) {
        this.topicChildren = topicChildren;
    }

    public void addNewTopicChild(TopicChild topicChild) {
        this.topicChildren.add(topicChild);
    }

    public void removeTopicChild(TopicChild topicChild) {
        this.topicChildren.remove(topicChild);
    }

    public void removeTopicChild(String id) {
        this.topicChildren.removeIf(s -> s.getId().equals(id));
    }
}
