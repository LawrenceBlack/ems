package com.stc.ems.repository;

import com.stc.ems.entity.EmsUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<EmsUser,String> {
    EmsUser findFirstByUserName(String userName);
}
