package com.stc.ems.entity;

import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.List;

public class UnitRoles {
    @DBRef
    private Unit unit;
    private List<Role> roles;

    public UnitRoles() {
    }

    public UnitRoles(Unit unit, List<Role> roles) {
        this.unit = unit;
        this.roles = roles;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
