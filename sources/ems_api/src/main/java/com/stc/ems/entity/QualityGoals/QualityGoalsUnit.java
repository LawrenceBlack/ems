package com.stc.ems.entity.QualityGoals;


import com.stc.ems.entity.Unit;
import org.springframework.data.annotation.Id;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document
//Mục tiêu chất lượng đơn vị
public class QualityGoalsUnit {
    @Id
    private String id;
    @DBRef
    private Unit unit;

    private String year;

    private List<TopicParent> listTopicParent;

    public QualityGoalsUnit() {
    }

    public QualityGoalsUnit(Unit unit, String year) {
        this.unit = unit;
        this.year = year;
        this.listTopicParent = new ArrayList<>();
    }

    public QualityGoalsUnit(Unit unit, String year, List<TopicParent> listTopicParent) {
        this.unit = unit;
        this.year = year;
        this.listTopicParent = listTopicParent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public String  getYear() {
        return year;
    }

    public void setYear(String  year) {
        this.year = year;
    }

    public List<TopicParent> getListTopicParent() {
        return listTopicParent;
    }

    public void setListTopicParent(List<TopicParent> listTopicParent) {
        this.listTopicParent = listTopicParent;
    }
    public void addNewTopicParent(TopicParent topicParent){
        this.listTopicParent.add(topicParent);
    }
    public void removeTopicParent(TopicParent topicParent){
        this.listTopicParent.remove(topicParent);
    }
    public void removeTopicParent(String id){
        this.listTopicParent.removeIf(s->s.getId().equals(id));
    }

}
