package com.stc.ems.controller;


import com.stc.ems.repository.QualityGoalUnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/quanlitygoalunit")
public class QualityGoalUnitController {

    @Autowired
    private QualityGoalUnitRepository qualityGoalUnitRepository;

    @GetMapping
    public ResponseEntity<?> getQuanlityByUnitAndYear(String unitId,String year ){
        return new ResponseEntity<>(qualityGoalUnitRepository.findFirstByUnitIdAndYear(unitId,year),HttpStatus.OK);
    }
}
