package com.stc.ems.dto.TokenDTO;

import java.util.List;

public class ResponseTokenDTO {
    private int statusCode;
    private String token;
    private String error;
    private List<String> roles;

    public ResponseTokenDTO(int statusCode, String token, List<String> roles, String error) {
        this.statusCode = statusCode;
        this.token = token;
        this.error = error;
        this.roles = roles;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}
