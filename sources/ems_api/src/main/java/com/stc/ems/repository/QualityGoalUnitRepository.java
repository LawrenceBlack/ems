package com.stc.ems.repository;

import com.stc.ems.entity.QualityGoals.QualityGoalsUnit;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface QualityGoalUnitRepository extends MongoRepository<QualityGoalsUnit,String> {

    QualityGoalsUnit findFirstByUnitIdAndYear(String unitId,String year);

}
