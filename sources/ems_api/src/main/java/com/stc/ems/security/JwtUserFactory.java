package com.stc.ems.security;

import com.stc.ems.entity.EmsUser;
import com.stc.ems.entity.Role;
import com.stc.ems.entity.UnitRoles;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class JwtUserFactory {
    public JwtUserFactory() {
    }
    public static JwtUser create(EmsUser emsUser,String unitId){
        return new JwtUser(
                emsUser.getUserName(),
                emsUser.getUserName(),
                mapToGrantedAuthorities(emsUser.getUnitRoles(),unitId),
                emsUser.isEnabled()
        );
    }
    private static List<GrantedAuthority> mapToGrantedAuthorities(List<UnitRoles> unitRoles,String unitId) {
        UnitRoles unitrole=unitRoles.stream().filter(o->o.getUnit().getId().equals(unitId)).findFirst().get();
            return unitrole.getRoles().stream()
                    .map(authority -> new SimpleGrantedAuthority(authority.getName()))
                    .collect(Collectors.toList());
    }
}
