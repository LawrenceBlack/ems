package com.stc.ems.dto;

import java.util.List;

public class ResponseVerifyTokenDTO {
    private int statusCode;
    private boolean isVerify;
    private String tokenVerify;
    private String tokenRefresh;
    private List<String> unitsId;
    private String error;

    public ResponseVerifyTokenDTO() {
    }

    public ResponseVerifyTokenDTO(int statusCode, boolean isVerify, List<String> unitsId,String tokenVerify,String tokenRefresh, String error) {
        this.statusCode = statusCode;
        this.isVerify = isVerify;
        this.tokenVerify=tokenVerify;
        this.tokenRefresh=tokenRefresh;
        this.unitsId = unitsId;
        this.error = error;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public List<String> getUnitsId() {
        return unitsId;
    }

    public void setUnitsId(List<String> unitsId) {
        this.unitsId = unitsId;
    }

    public boolean isVerify() {
        return isVerify;
    }

    public void setVerify(boolean verify) {
        isVerify = verify;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTokenVerify() {
        return tokenVerify;
    }

    public void setTokenVerify(String tokenVerify) {
        this.tokenVerify = tokenVerify;
    }
}
