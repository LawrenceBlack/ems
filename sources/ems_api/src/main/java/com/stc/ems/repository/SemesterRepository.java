package com.stc.ems.repository;


import com.stc.ems.entity.Semester;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SemesterRepository extends MongoRepository<Semester,String> {
}
