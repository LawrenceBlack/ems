package com.stc.ems.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtTokenUtil {

    private static final String CLAIM_USERNAME="username";
    private static final String CLAIM_EMAIL="email";
    private static final String CLAIM_KEY_ROLE ="roles";
    private static final String CLAIM_ACTIVE_UNIT="unit";
    private static final int DAY_DURATION = 1000 * 60 * 60 * 24 * 7; //90 days
    private static final long DAY_DURATION_VERIFYTOKEN=1000 * 60 * 30; //30 min
    private static final long DAY_DURATION_REFRESH_VERIFYTOKEN=1000 * 60 * 60 * 24 *30; //30 days

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.claim.secret}")
    private String valueSecret;

    public String getUserNameFromToken(String token){
        String username;
        try {
            final Claims claims=getClaimsFromToken(token);
            username=claims.get(CLAIM_USERNAME).toString();
        }
        catch (Exception e){
            username=null;
        }
        return username;
    }

    public String getActiveUnitIdFromToken(String token){
        String activeUnitId;
        try {
            final Claims claims=getClaimsFromToken(token);
            activeUnitId=claims.get(CLAIM_ACTIVE_UNIT).toString();
        }
        catch (Exception e){
            activeUnitId=null;
        }
        return activeUnitId;
    }

    public String getEmailFromToken(String token){
        String email;
        try {
            final Claims claims=getClaimsFromToken(token);
            email=claims.get(CLAIM_EMAIL).toString();
        }
        catch (Exception e){
            email=null;
        }
        return email;
    }

    public Date getExpirationDateFromToken(String token) {
        Date expiration;
        try {
            final Claims claims = getClaimsFromToken(token);
            expiration = claims.getExpiration();
        } catch (Exception e) {
            expiration = null;
        }
        return expiration;
    }

    public String generatedToken(UserDetails userDetails,String activeUnitId){
        return Jwts.builder()
                .claim(CLAIM_USERNAME,userDetails.getUsername())
                .claim(CLAIM_KEY_ROLE,userDetails.getAuthorities())
                .claim(CLAIM_ACTIVE_UNIT,activeUnitId)
                .setExpiration(new Date(new Date().getTime() + DAY_DURATION))
                .signWith(SignatureAlgorithm.HS512,secret)
                .compact();
    }

    public String generatedToken(String email){
        return Jwts.builder()
                .claim(CLAIM_EMAIL,email+"-"+valueSecret)
                .setExpiration(new Date(new Date().getTime()+DAY_DURATION_VERIFYTOKEN))
                .signWith(SignatureAlgorithm.HS512,secret)
                .compact();
    }

    public String generatedRefreshToken(String email){
        return Jwts.builder()
                .claim(CLAIM_EMAIL,email+"-"+valueSecret)
                .setExpiration(new Date(new Date().getTime()+DAY_DURATION_REFRESH_VERIFYTOKEN))
                .signWith(SignatureAlgorithm.HS512,secret)
                .compact();
    }

    public boolean isTokenExpired(String token){
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    private Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }

    public boolean validateEmailFromToken (String email){
       String[] splitStr=email.split("-");
       if(splitStr[1].equals(valueSecret))
           return true;
       return false;
    }

    public Boolean validateToken(String token, UserDetails userDetails){
        JwtUser jwtUser=(JwtUser)userDetails;
        final String username=getUserNameFromToken(token);
        return username.equals(jwtUser.getUsername())&& !isTokenExpired(token);
    }
}
