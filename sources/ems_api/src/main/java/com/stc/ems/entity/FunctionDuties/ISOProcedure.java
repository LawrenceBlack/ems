package com.stc.ems.entity.FunctionDuties;

import java.util.List;

public class ISOProcedure {
    private String id;
    private String name;
    private List<String> listUrlFile;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getListUrlFile() {
        return listUrlFile;
    }

    public void setListUrlFile(List<String> listUrlFile) {
        this.listUrlFile = listUrlFile;
    }
}
