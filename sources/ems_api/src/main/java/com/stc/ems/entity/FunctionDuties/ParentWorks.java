package com.stc.ems.entity.FunctionDuties;


import java.util.Date;
import java.util.List;

public class ParentWorks {

    private String id;
    private String workName;
    private List<ChildWorks> childWorks;
    private String userCreated;
    private Date dateCreate;
    private String lastUserEdit;
    private Date lastDateEdit;
    private List<ChangeLog> changeLogs;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWorkName() {
        return workName;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    public List<ChildWorks> getChildWorks() {
        return childWorks;
    }

    public void setChildWorks(List<ChildWorks> childWorks) {
        this.childWorks = childWorks;
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getLastUserEdit() {
        return lastUserEdit;
    }

    public void setLastUserEdit(String lastUserEdit) {
        this.lastUserEdit = lastUserEdit;
    }

    public List<ChangeLog> getChangeLogs() {
        return changeLogs;
    }

    public void setChangeLogs(List<ChangeLog> changeLogs) {
        this.changeLogs = changeLogs;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getLastDateEdit() {
        return lastDateEdit;
    }

    public void setLastDateEdit(Date lastDateEdit) {
        this.lastDateEdit = lastDateEdit;
    }
}
