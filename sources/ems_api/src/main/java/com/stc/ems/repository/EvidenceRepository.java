package com.stc.ems.repository;

import com.stc.ems.entity.Evidence;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EvidenceRepository extends MongoRepository<Evidence,String> {
}
