package com.stc.ems.dto.TokenDTO;

public class RequestTokenDTO {
    private String email;
    private String unitId;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }
}
