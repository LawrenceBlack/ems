package com.stc.ems.controller;

import com.stc.ems.dto.ResponseRefreshTokenDTO;
import com.stc.ems.dto.ResponseVerifyTokenDTO;
import com.stc.ems.dto.TokenDTO.RequestTokenDTO;
import com.stc.ems.dto.TokenDTO.ResponseTokenDTO;
import com.stc.ems.entity.EmsUser;
import com.stc.ems.entity.UnitRoles;
import com.stc.ems.repository.UserRepository;
import com.stc.ems.security.JwtTokenUtil;
import com.stc.ems.security.JwtUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.parameters.P;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 */
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Value("${google.verifyUrl}")
    private String urlVerifyTokenGoogle;
    private RestTemplate restTemplate = new RestTemplate();

    @PostMapping(value = "/refresh_token")
    public ResponseEntity<ResponseRefreshTokenDTO> refreshToken(@RequestHeader(required = true, value = "refresh_token") String refreshToken) {
        try {
            String email = jwtTokenUtil.getEmailFromToken(refreshToken);
            if (email == null || !jwtTokenUtil.validateEmailFromToken(email)) {
                throw new Exception("Unknown token");
            }
            if (jwtTokenUtil.isTokenExpired(refreshToken)) {
                throw new Exception("Token is expired");
            }
            String jwt = jwtTokenUtil.generatedToken(email.split("-")[0]);
            return new ResponseEntity<>(new ResponseRefreshTokenDTO(HttpStatus.OK.value(), jwt, null), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseRefreshTokenDTO(HttpStatus.BAD_REQUEST.value(), null, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Action: getJWT
     * Method: GET
     *
     * @param token
     * @param unitId
     * @return ResponseTokenDTO
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseTokenDTO> getJwt(@RequestHeader(required = true, value = "verify_token") String token, @RequestParam(value = "unitId") String unitId) {
        try {
            String email = jwtTokenUtil.getEmailFromToken(token);
            if (email == null || !jwtTokenUtil.validateEmailFromToken(email)) {
                throw new Exception("Unknown token");
            }
            if (jwtTokenUtil.isTokenExpired(token)) {
                throw new Exception("Token is expired");
            }
            EmsUser emsUser = userRepository.findFirstByUserName(email.split("-")[0]);
            //verify email in db
            if (emsUser == null) {
                throw new Exception("Email not found in database");
            } else {
                List<String> lstUnitId = emsUser.getUnitRoles().parallelStream().map(id -> id.getUnit().getId()).collect(Collectors.toList());
                if (lstUnitId.indexOf(unitId) == -1) {
                    throw new Exception("Email don't have permission to access unit");
                } else {
                    UserDetails userDetails = getUserAuthentication(emsUser, unitId);
                    String jwtToken = jwtTokenUtil.generatedToken(userDetails, unitId);
                    // nhan danh sach roleNames tu danh sach UnitRoles
                    UnitRoles unitRoles = emsUser.getUnitRoles().stream().filter(o -> o.getUnit().getId().equals(unitId)).findFirst().get();
                    List<String> roleNames = unitRoles.getRoles().stream().map(o -> o.getName()).collect(Collectors.toList());
                    return new ResponseEntity<>(new ResponseTokenDTO(HttpStatus.OK.value(), jwtToken, roleNames, null), HttpStatus.OK);
                }
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(new ResponseTokenDTO(HttpStatus.BAD_REQUEST.value(), null, null, ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/verify_token")
    public ResponseEntity<ResponseVerifyTokenDTO> verifyToken(@RequestHeader(required = true, value = "auth-token") String authToken) {

        String urlRequest = urlVerifyTokenGoogle + authToken;

        try {
            if (authToken == null || authToken.isEmpty()) {
                throw new Exception("Auth Token can't be null");
            }
            ResponseEntity<HashMap> responseEntity = restTemplate.exchange(urlRequest, HttpMethod.GET, null, HashMap.class);
            if (responseEntity.getStatusCode() != HttpStatus.OK) {
                throw new Exception("Can't verify token");
            }
            HashMap<String, String> map = responseEntity.getBody();
            if (Boolean.parseBoolean(map.get("email_verified")) != true) {
                throw new Exception("Token isn't verify by google");
            }
            String userName = map.get("email");
            EmsUser emsUser = userRepository.findFirstByUserName(userName);
            if (emsUser == null) {
                throw new Exception("Email not found in database");
            }
//            if(!isHcmUteEmail(userName)){
//                throw new Exception("Email isn't email HCMUTE");
//            }
            List<String> unitId = emsUser.getUnitRoles().stream()
                    .map(id -> id.getUnit().getId())
                    .collect(Collectors.toList());

            String jwt = jwtTokenUtil.generatedToken(userName);
            String jwtRefresh = jwtTokenUtil.generatedRefreshToken(userName);

            return new ResponseEntity<>(new ResponseVerifyTokenDTO(HttpStatus.OK.value(), true, unitId, jwt, jwtRefresh, null), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(new ResponseVerifyTokenDTO(HttpStatus.BAD_REQUEST.value(), false, null, null, null, ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    private JwtUser getUserAuthentication(EmsUser emsUser, String unitId) throws Exception {

        Optional<UnitRoles> unitRoles = emsUser.getUnitRoles().stream().filter(o -> o.getUnit().getId().equals(unitId)).findFirst();
        if (unitRoles.isPresent()) {
            List<GrantedAuthority> authorities = unitRoles.get().getRoles().stream()
                    .map(authority -> new SimpleGrantedAuthority(authority.getName()))
                    .collect(Collectors.toList());
            return new JwtUser(emsUser.getUserName(), emsUser.getUserName(), authorities, emsUser.isEnabled());
        } else
            throw new Exception("Can't find unit roles");
    }

    private boolean isHcmUteEmail(String email) {
        String[] emailSplit = email.split("@");
        if (emailSplit[1].equals("hcmute.edu.vn")) {
            return true;
        }
        return false;
    }

}
