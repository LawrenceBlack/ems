package com.stc.ems.entity.FunctionDuties;

import com.stc.ems.entity.Evidence;
import com.stc.ems.entity.StoreType;

import java.util.List;

public class Storage {
    private StoreType storeType;
    private List<Evidence> evidences;

    public StoreType getStoreType() {
        return storeType;
    }

    public void setStoreType(StoreType storeType) {
        this.storeType = storeType;
    }

    public List<Evidence> getEvidences() {
        return evidences;
    }

    public void setEvidences(List<Evidence> evidences) {
        this.evidences = evidences;
    }
}
