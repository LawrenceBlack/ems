package com.stc.ems.security;

import com.stc.ems.entity.EmsUser;
import com.stc.ems.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service("customUserDetailsService")
public class JwtAuthenticationServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        String[] splitString=s.split("-");
        EmsUser emsUser=userRepository.findFirstByUserName(splitString[0]);

        if(emsUser ==null){
            throw new UsernameNotFoundException("No user found with username : "+s);
        }
        else{
            return JwtUserFactory.create(emsUser,splitString[1]);
        }

    }
}
