package com.stc.ems.entity.QualityGoals;

import com.stc.ems.entity.Comment;
import com.stc.ems.entity.Evidence;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

//Kế hoạch thực hiện
public class ImplementationPlan {
    private String id;
    private String content;
    private Date timeStart;
    private Date timeEnd;
    @DBRef
    private List<Evidence> listEvidence; //Minh chứng

    private List<Comment> listComments;

    public ImplementationPlan() {
    }

    public ImplementationPlan(String content, Date timeStart, Date timeEnd) {
        this.id=UUID.randomUUID().toString();
        this.content = content;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.listEvidence =new ArrayList<>();
    }

    public ImplementationPlan(String content, Date timeStart, Date timeEnd, List<Evidence> listEvidence) {
        this.id=UUID.randomUUID().toString();
        this.content = content;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.listEvidence = listEvidence;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(Date timeStart) {
        this.timeStart = timeStart;
    }

    public Date getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(Date timeEnd) {
        this.timeEnd = timeEnd;
    }

    public List<Evidence> getListEvidence() {
        return listEvidence;
    }

    public void setListEvidence(List<Evidence> listEvidence) {
        this.listEvidence = listEvidence;
    }
    public void addNewEvidence(Evidence evidence){
        this.listEvidence.add(evidence);
    }

    public List<Comment> getListComments() {
        return listComments;
    }

    public void setListComments(List<Comment> listComments) {
        this.listComments = listComments;
    }
    public void addNewComment(Comment comment){
        this.listComments.add(comment);
    }
    public void removeComment(Comment comment){
        this.listComments.remove(comment);
    }
    public void removeComment(String location){
        this.listComments.removeIf(s->s.getLocation().equals(location));
    }
}
