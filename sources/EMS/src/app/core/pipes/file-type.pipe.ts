import { Pipe, PipeTransform } from '@angular/core';
const FILE_EXTENTION_ICON = [
    '<i class="fa fa-file-pdf-o"></i>',
    '<i class="fa fa-file-word-o"></i>',
    '<i class="fa fa-file-excel-o"></i>',
    '<i class="fa fa-file-image-o"></i>',
    '<i class="fa fa-file-o"></i>',
    '<i class="fa fa-file-archive-o"></i>',
    '<i class="fa fa-file-powerpoint-o"></i>',
    '<i class="fa fa-file-video-o"></i>',
    '<i class="fa fa-file-audio-o"></i>'
];
@Pipe({
    name: 'fileType'
})
export class FileTypePipe implements PipeTransform {
    transform(typeFile: string, longForm: boolean): string {
        let extensionFile = (typeFile.split('.').pop()).toLowerCase();
        console.log(extensionFile);
        let result: string;
        const FILE_PDF = 'pdf';
        const FILE_DOC = 'doc';
        const FILE_DOCX = 'docx';
        const FILE_EXCEL = 'xlsx';
        const FILE_IMAGE_PNG = 'png';
        const FILE_IMAGE_JPG = 'jpg';
        const FILE_ZIP = 'zip';
        const FILE_RAR = 'rar';
        const FILE_VIDEO_MP4 = 'mp4';
        const FILE_VIDEO_MKV = 'mkv';
        const FILE_AUDIO_mp3 = 'mp3';
        switch (extensionFile) {
            case FILE_PDF:
                result = FILE_EXTENTION_ICON[0];
                break;
            case FILE_DOC:
                result = FILE_EXTENTION_ICON[1];
                break;
            case FILE_DOCX:
                result = FILE_EXTENTION_ICON[1];
                break;
            case FILE_EXCEL:
                result = FILE_EXTENTION_ICON[2];
                break;
            case FILE_IMAGE_JPG:
                result = FILE_EXTENTION_ICON[3];
                break;
            case FILE_IMAGE_PNG:
                result = FILE_EXTENTION_ICON[3];
                break;
            case FILE_RAR:
                result = FILE_EXTENTION_ICON[5];
                break;
            case FILE_ZIP:
                result = FILE_EXTENTION_ICON[5];
                break;
            case FILE_VIDEO_MP4:
                result = FILE_EXTENTION_ICON[7];
                break;
            case FILE_VIDEO_MKV:
                result = FILE_EXTENTION_ICON[7];
                break;
            default:
                result = FILE_EXTENTION_ICON[4];
                break;
        }

        return result;
    }

}