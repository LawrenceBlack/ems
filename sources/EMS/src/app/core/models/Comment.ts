export class Comment {
    private _id: string;
    private _location: string;
    private _content: string;
    private _userComment: string;
    private _timeCommnet: Date;
    private _timeLastEdit: Date;

    constructor() { }

    public get id(): string {
        return this._id;
    }
    public set id(value: string) {
        this._id = value;
    }
    public get location(): string {
        return this._location;
    }
    public set location(value: string) {
        this._location = value;
    }
    public get content(): string {
        return this._content;
    }
    public set content(value: string) {
        this._content = value;
    }
    public get userComment(): string {
        return this._userComment;
    }
    public set userComment(value: string) {
        this._userComment = value;
    }
    public get timeCommnet(): Date {
        return this._timeCommnet;
    }
    public set timeCommnet(value: Date) {
        this._timeCommnet = value;
    }
    public get timeLastEdit(): Date {
        return this._timeLastEdit;
    }
    public set timeLastEdit(value: Date) {
        this._timeLastEdit = value;
    }
}
