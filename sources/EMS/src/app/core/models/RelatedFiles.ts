import { File } from './File';
export class RelatedFiles {
    private _id: string;
    private _files: Array<File>;

    constructor() { }

    public get id(): string {
        return this._id;
    }
    public set id(value: string) {
        this._id = value;
    }
    public get files(): Array<File> {
        return this._files;
    }
    public set files(value: Array<File>) {
        this._files = value;
    }
}
