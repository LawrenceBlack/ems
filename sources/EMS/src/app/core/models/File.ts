export class File {
    private _id: string;
    private _fullName: string;
    private _normalizedName: string;
    private _userUpload: string;
    private _dateUpload: Date;
    private _unit: string;
    private _type: string;
    private _size: string;
    private _view: number;
    private _download: number;

    constructor() { }

    public get id(): string {
        return this._id;
    }
    public set id(value: string) {
        this._id = value;
    }
    public get fullName(): string {
        return this._fullName;
    }
    public set fullName(value: string) {
        this._fullName = value;
    }
    public get normalizedName(): string {
        return this._normalizedName;
    }
    public set normalizedName(value: string) {
        this._normalizedName = value;
    }
    public get userUpload(): string {
        return this._userUpload;
    }
    public set userUpload(value: string) {
        this._userUpload = value;
    }
    public get dateUpload(): Date {
        return this._dateUpload;
    }
    public set dateUpload(value: Date) {
        this._dateUpload = value;
    }
    public get unit(): string {
        return this._unit;
    }
    public set unit(value: string) {
        this._unit = value;
    }
    public get type(): string {
        return this._type;
    }
    public set type(value: string) {
        this._type = value;
    }
    public get size(): string {
        return this._size;
    }
    public set size(value: string) {
        this._size = value;
    }
    public get view(): number {
        return this._view;
    }
    public set view(value: number) {
        this._view = value;
    }
    public get download(): number {
        return this._download;
    }
    public set download(value: number) {
        this._download = value;
    }

}
