export class ISOProcedure {
    private _id: string;
    private _name: string;
    private _listUrlFile: Array<string>;

    constructor() { }

    public get id(): string {
        return this._id;
    }
    public set id(value: string) {
        this._id = value;
    }
    public get name(): string {
        return this._name;
    }
    public set name(value: string) {
        this._name = value;
    }
    public get listUrlFile(): Array<string> {
        return this._listUrlFile;
    }
    public set listUrlFile(value: Array<string>) {
        this._listUrlFile = value;
    }
}
