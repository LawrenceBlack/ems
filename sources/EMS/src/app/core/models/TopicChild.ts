import { Plan } from './plan';
export class TopicChild {
    private _id: string;
    private _stt: number;
    private _title: string;
    private _kpis: string;
    private _boss: string;
    private _department: string;
    private _department_im: string;
    private _plans: Array<Plan>;
    private _idComments: Array<string>;

    constructor() { }

    public get id(): string {
        return this._id;
    }
    public set id(value: string) {
        this._id = value;
    }
    public get stt(): number {
        return this._stt;
    }
    public set stt(value: number) {
        this._stt = value;
    }
    public get title(): string {
        return this._title;
    }
    public set title(value: string) {
        this._title = value;
    }
    public get kpis(): string {
        return this._kpis;
    }
    public set kpis(value: string) {
        this._kpis = value;
    }
    public get boss(): string {
        return this._boss;
    }
    public set boss(value: string) {
        this._boss = value;
    }
    public get department(): string {
        return this._department;
    }
    public set department(value: string) {
        this._department = value;
    }
    public get department_im(): string {
        return this._department_im;
    }
    public set department_im(value: string) {
        this._department_im = value;
    }
    public get plans(): Array<Plan> {
        return this._plans;
    }
    public set plans(value: Array<Plan>) {
        this._plans = value;
    }
    public get idComments(): Array<string> {
        return this._idComments;
    }
    public set idComments(value: Array<string>) {
        this._idComments = value;
    }
}
