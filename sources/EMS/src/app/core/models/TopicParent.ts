import { TopicChild } from './TopicChild';
export class TopicParent {
    private _id: string;
    private _roman: string;
    private _title: string;
    private _topicChilds: Array<TopicChild>;


    constructor() { }


    public get id(): string {
        return this._id;
    }
    public set id(value: string) {
        this._id = value;
    }
    public get roman(): string {
        return this._roman;
    }
    public set roman(value: string) {
        this._roman = value;
    }
    public get title(): string {
        return this._title;
    }
    public set title(value: string) {
        this._title = value;
    }
    public get topicChilds(): Array<TopicChild> {
        return this._topicChilds;
    }
    public set topicChilds(value: Array<TopicChild>) {
        this._topicChilds = value;
    }

}
