export class Plan {
    private _id: string;
    private _title: string;
    private _startTime: string;
    private _endTime: string;

    constructor() { }

    public get id(): string {
        return this._id;
    }
    public set id(value: string) {
        this._id = value;
    }
    public get title(): string {
        return this._title;
    }
    public set title(value: string) {
        this._title = value;
    }
    public get startTime(): string {
        return this._startTime;
    }
    public set startTime(value: string) {
        this._startTime = value;
    }
    public get endTime(): string {
        return this._endTime;
    }
    public set endTime(value: string) {
        this._endTime = value;
    }
}
