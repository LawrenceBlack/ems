export class MessageConstant {
    public static MSG_LOGIN_DISCONNECT = 'Hết thời gian đăng nhập';
    public static MSG_ERROR_LOGIN_GOOGLE = 'Lỗi xác thực dịch vụ google';
    public static MSG_LOGIN_GOOGLE_SUCCESS = 'Bạn đã đăng nhập thành công';
    public static MSG_ERROR_LOGIN_SERVER = 'Email không có quyền truy cập';
    public static MSG_ERROR_UNIT_ROLE = 'Bạn không có quyền truy cập đơn vị';
    public static MSG_CHOOSE_UNIT = 'Bạn vui lòng chọn phòng ban.';
    public static MSG_ERROR_ACCESS = 'Lỗi truy cập';

}

export class MessageForm {
    public static MSG_FIELD_REQUIRED = 'Trường này không được để trống';
}
