export class SystemConstant {
    public static CURRENT_USER = 'CURRENT_USER';
    public static TOKEN_KEY = 'a5smm_utoken';
    public static TOKEN_GOOGLE = 'a12wU125_qx01';
    public static ROLE = 'role';
    public static PREFIX = 'Bearer ';
    public static TOKEN_HEADER_KEY = 'Authorization';
    public static SYSTEM_ROLE_USER = 'user';
    public static SYSTEM_ROLE_ADMIN = 'admin';
    public static SYSTEM_ROLE_SUPER_ADMIN = 'super_admin';
    public static USERNAME_DEFAULT = 'unknown user';
    public static SECRECT_KEY = 'U2FsdGVkX1';
    public static ROLE_KEY = 'ceUUWxsyhV';
    public static LIST_UNITS_ROLE = 'units_role';
    public static CLIENT_ID_GOOGLE = '527170146226-q57bpsv527vm3ekdekcs46d9eaugg3m5.apps.googleusercontent.com';
}
