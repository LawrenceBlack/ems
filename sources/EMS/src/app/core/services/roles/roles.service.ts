import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { UrlConstant } from '../../commons/url.constant';
import { tap } from 'rxjs/operators';
import { ShareService } from '../share.service';
@Injectable()
export class RolesService {

    private listRoles: Array<any> = [];
    private httpOptions;

    constructor(private _http: HttpClient, private shareService: ShareService) {
        // get list role unit
        shareService._listRoleUnitsId.subscribe(listRoles => this.listRoles = listRoles);
    }

    getAllRoles(): Observable<any> {
        this.httpOptions = new Headers({
            'Content-Type': 'application/json',
        });
        return this._http.post<Observable<any>>(UrlConstant.API + 'units/listId', this.listRoles, this.httpOptions)
            .pipe(
                tap((data) => {
                    return data;
                }, (err) => {
                    return err;
                })
            );
    }
}
