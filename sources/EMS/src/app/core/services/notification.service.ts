import { Injectable } from '@angular/core';

@Injectable()
export class NotifiCationService {

    constructor() { }

    alertSuccess(message: string) {
        $.notify({
            icon: 'pe-7s-bell',
            message: message

        }, {
            type: 'success',
            timer: 2000
        });
    }

    alertError(message: string) {
        $.notify({
            icon: 'pe-7s-bell',
            message: message

        }, {
            type: 'danger',
            timer: 2000
        });
    }
}
