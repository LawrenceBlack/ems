import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { UrlConstant } from '../commons/url.constant';

@Injectable()
export class ShareService {

    private listRoleUnitsId = new BehaviorSubject<Array<any>>([]);
    _listRoleUnitsId = this.listRoleUnitsId.asObservable();

    private tokenStep_1 = new BehaviorSubject<string>(null);
    _tokenStep_1 = this.listRoleUnitsId.asObservable();

    private tokenStepGoogle = new BehaviorSubject<string>(null);
    _tokenStepGoogle = this.tokenStepGoogle.asObservable();


    constructor() { }

    setListRoleUnitsId(listRoles: Array<any>) {
        this.listRoleUnitsId.next(listRoles);
    }
    setTokenStep_1(token: string) {
        this.tokenStep_1.next(token);
    }
    getTokenStep_1(): string {
        return this.tokenStep_1.getValue();
    }

    setTokenStepGoogle(token: string) {
        this.tokenStepGoogle.next(token);
    }
    getTokenStepGoogle(): string {
        return this.tokenStepGoogle.getValue();
    }

}
