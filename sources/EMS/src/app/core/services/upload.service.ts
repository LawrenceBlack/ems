import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { ConfigService } from './config.service';
import { NotifiCationService } from './notification.service';
import { SystemConstant } from '../commons/system.constant';
import { MessageConstant } from '../commons/message.constant';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class UploadService {
    public responseData: any;
    constructor(
        private _http: HttpClient,
        private configService: ConfigService,
        private _notifycationService: NotifiCationService,
        private _router: Router
    ) { }

    postWithFile(url: string, postData: any, files: any, nameFile: string) {
        let formData: FormData = new FormData();
        formData.append(nameFile, files, files.name);
        if (postData !== '' && postData !== undefined && postData !== null) {
            for (var property in postData) {
                if (postData.hasOwnProperty(property)) {
                    formData.append(property, postData[property]);
                }
            }
        }
        let returnReponse = new Promise((resolve, reject) => {
            return this._http.post(this.configService.getApiURL() + url, formData)
                .subscribe(
                    res => {
                        this.responseData = res;
                        resolve(this.responseData);
                    },
                    err => {
                        this.handleError(err);
                    }
                );
        });
        return returnReponse;
    }

    accessToken() {
        const strToken = JSON.parse(localStorage.getItem(SystemConstant.CURRENT_USER));
        if (strToken) {
            let httpOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + strToken.token
            };
            return httpOptions;
        }
    }

    private navigateToLogin() {
        this._router.navigate(['/admin/login']);
    }

    handleError(error: any) {
        if (error.status === 401) {
            localStorage.removeItem(SystemConstant.CURRENT_USER);
            this._notifycationService.alertError(MessageConstant.MSG_LOGIN_DISCONNECT);
            this.navigateToLogin();
        } else if (error.status === 403) {
            localStorage.removeItem(SystemConstant.CURRENT_USER);
            this._notifycationService.alertError(MessageConstant.MSG_LOGIN_DISCONNECT);
            this.navigateToLogin();
        } else {
            console.log(error);
            this._notifycationService.alertError(MessageConstant.MSG_LOGIN_DISCONNECT);
        }
    }

    private extractData(res: Response) {
        const body = res.json();
        return body || {};
    }
}
