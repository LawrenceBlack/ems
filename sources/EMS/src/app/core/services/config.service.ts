import { Injectable } from '@angular/core';
import { UrlConstant } from '../commons/url.constant';
@Injectable()
export class ConfigService {
    apiURL: string = '';
    constructor() {
        // this.apiURL = 'http://localhost:8000/api/';
        this.apiURL = UrlConstant.API;
    }

    getApiURL() {
        console.log(this.apiURL);
        return this.apiURL;
    }
}
