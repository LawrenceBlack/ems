import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, catchError, retry } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { UrlConstant } from '../commons/url.constant';
import { AuthService, SocialUser, GoogleLoginProvider } from 'angular4-social-login';
import { UtilService } from '../services/util.service';
import { SystemConstant } from '../commons/system.constant';
import { JwtHelper } from 'angular2-jwt';

@Injectable()
export class AuthenticationService {
    token = {
        refresh_token: 'refreshtokencode',
        exp: new Date((new Date().getDate() + 1)),
        access_token: {
            username: 'user',
            roles: ['Admin', 'RegisteredUser', 'Super User']
        }
    };
    private httpOptions;

    tokenKey: string = 'a5smm_utoken';

    private helper: JwtHelper;


    constructor(
        private _httpClient: HttpClient,
        private authGoogleService: AuthService,
        private utilService: UtilService,
        private router: Router) { this.helper = new JwtHelper(); }

    login(username, password) {
        this.setToken(this.token);
        this.router.navigate(['admin', 'dashboard']);
    }

    logout() {
        this.utilService.removeToken(SystemConstant.TOKEN_KEY);
        this.utilService.removeToken(SystemConstant.ROLE_KEY);
        this.utilService.removeToken(SystemConstant.TOKEN_GOOGLE);
        window.localStorage.clear();
        // this.signOutWithGoogle();
        this.router.navigate(['login']);
    }

    getToken() {
        return JSON.parse(localStorage.getItem(this.tokenKey));
    }

    setToken(token) {
        localStorage.setItem(this.tokenKey, JSON.stringify(token));
    }

    getAccessToken() {
        return JSON.parse(localStorage.getItem(this.tokenKey))['access_token'];
    }

    isAuthenticated(): boolean {
        const token = this.utilService.getLocalStorage(SystemConstant.TOKEN_KEY);
        // const decodedToken = this.helper.decodeToken(token);
        // const expirationDate = this.helper.getTokenExpirationDate(token);
        if (token) {
            const isExpired = this.helper.isTokenExpired(token);
            if (isExpired) {
                this.utilService.removeToken(SystemConstant.TOKEN_KEY);
                this.utilService.removeToken(SystemConstant.ROLE_KEY);
                this.utilService.removeToken(SystemConstant.TOKEN_GOOGLE);
                window.localStorage.clear();
                this.signOutWithGoogle();
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    refreshToken() {
        this.token.exp = new Date((new Date().getDate() + 1));
        this.setToken(this.token);
    }

    verifyToken(token: string): Observable<any> {
        // tao header
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'auth-token': token
            })
        };
        return this._httpClient.get<Observable<any>>(UrlConstant.API + 'auth/verify_token', this.httpOptions)
            .pipe(
                tap(
                    (data) => {
                        // console.log('Login step 1', data);
                        return data;
                    }, (err) => {
                        return err;
                    }
                )
            );
    }

    signInGoogle(): Promise<SocialUser> {
        return this.authGoogleService.signIn(GoogleLoginProvider.PROVIDER_ID);
    }

    signOutWithGoogle(): Promise<any> {
        return this.authGoogleService.signOut();
    }

    getSocialUserGoogle(): Observable<SocialUser> {
        return this.authGoogleService.authState;
    }

    getTokenStep_2(token: string, unitId: String): Observable<any> {
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'verify_token': token
            })
        };
        return this._httpClient.get<Observable<any>>(UrlConstant.API + 'auth?unitId=' + unitId, this.httpOptions)
            .pipe(
                tap(
                    (data) => {
                        return data;
                    },
                    (err) => {
                        console.log(err);
                        return err;
                    }
                )
            );
    }

    /**
     *   @Number return: 0-> login, 1-> client, 2->department, 3->dbcl
    */
    checkUtilRole(): number {
        const role_hash = this.utilService.getLocalStorage(SystemConstant.ROLE_KEY);
        // if not exists unit role then return page roles
        if (!role_hash || role_hash === '' || role_hash === undefined) {
            return 0;
        } else {
            // decrypt role
            const role: string = this.utilService.decryptKey(role_hash, SystemConstant.SECRECT_KEY);
            if (role === SystemConstant.SYSTEM_ROLE_USER) {
                return 1;
            } else {
                if (role === SystemConstant.SYSTEM_ROLE_ADMIN) {
                    return 2;
                } else {
                    if (role === SystemConstant.SYSTEM_ROLE_SUPER_ADMIN) {
                        return 3;
                    } else { return 0; }
                }
            }
        }
    }

    getTokenGoogleFromLocal(): boolean {
        const token = this.utilService.getLocalStorage(SystemConstant.TOKEN_GOOGLE);
        return token ? true : false;
    }
    getUnitsRoleFromLocal(): boolean {
        return this.utilService.getLocalStorage(SystemConstant.LIST_UNITS_ROLE) ? true : false;
    }
}
