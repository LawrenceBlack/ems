import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../authentication.service';
import { SocialUser } from 'angular4-social-login';
import { Observable } from 'rxjs/observable';


@Injectable()
export class AuthGuard implements CanActivate {

    isAuthenticated: boolean;
    userGoogle: SocialUser;
    role: number;

    constructor(private router: Router, private authen: AuthenticationService) {

    }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        // cach 3
        this.isAuthenticated = this.authen.isAuthenticated();
        if (this.isAuthenticated) {
            return true;
        } else {
            this.router.navigate(['login']);
            return false;
        }
        // cach 1
        // return new Promise((resolve, reject) => {
        //     Promise.resolve([this.authen.isAuthenticated()])
        //         .then((isAuthen) => {
        //             this.isAuthenticated = isAuthen[0];
        //             return Promise.resolve([this.authen.checkUtilRole()]);
        //         })
        //         .then((roles) => {
        //             this.role = roles[0];
        //             return Promise.resolve([this.authen.getSocialUserGoogle()]);
        //         })
        //         .then((socialUser) => {
        //             socialUser[0].subscribe(user => this.userGoogle = user);
        //             console.log(this.isAuthenticated, this.userGoogle, this.role);
        //             if (this.isAuthenticated) {
        //                 // check role
        //                 if (this.role === 0) {
        //                     console.log('roles');
        //                     this.router.navigate(['roles']).then(() => resolve(true));
        //                 }
        //                 if (this.role === 1) {
        //                     console.log('client');
        //                     this.router.navigate(['main/client']).then(() => resolve(true));
        //                 }
        //                 if (this.role === 2) {
        //                     console.log('department');
        //                     this.router.navigate(['main/department']).then(() => resolve(true));
        //                 }
        //                 if (this.role === 3) {
        //                     console.log('ems');
        //                     this.router.navigate(['main/ems']).then(() => resolve(true));
        //                 }
        //             } else {
        //                 console.log('login');
        //                 this.router.navigate(['roles']).then(() => resolve(false));
        //                 resolve(false);
        //                 // this.router.navigate(['login']);
        //             }
        //         })
        //         .catch((err) => {
        //             console.log(err);
        //             this.router.navigate(['login']);
        //             // resolve(true);
        //             return resolve(false);
        //         });
        // });
        // cach 2

        // return new Promise((resolve) => {
        //     this.isAuthenticated = this.authen.isAuthenticated();
        //     if (this.isAuthenticated) {
        //         resolve(true);
        //     } else {
        //         this.router.navigate(['login']);
        //         resolve(false);
        //     }
        // });

    }
}
