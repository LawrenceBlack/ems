import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { UtilService } from '../util.service';
import { SystemConstant } from '../../commons/system.constant';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private utilService: UtilService) { }

    intercept(req: HttpRequest<any>,
        next: HttpHandler): Observable<HttpEvent<any>> {
        const tokenKey = this.utilService.getLocalStorage(SystemConstant.TOKEN_KEY);

        if (tokenKey) {
            const cloned = req.clone({
                headers: req.headers.set(SystemConstant.TOKEN_HEADER_KEY,
                    SystemConstant.PREFIX + tokenKey)
            });
            return next.handle(cloned);
        } else {
            return next.handle(req);
        }
    }
}
