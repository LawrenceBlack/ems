import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../authentication.service';
import { SocialUser } from 'angular4-social-login';
import { Observable } from 'rxjs/observable';

@Injectable()
export class RoleAuthGuard implements CanActivate {

    constructor(private router: Router, private authen: AuthenticationService) { }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        const roleData = route.data.role;
        const role = this.authen.checkUtilRole();
        if (!this.authen.isAuthenticated() || role !== roleData) { // chu y
            console.log('Khong co quyen, chuyen ve login');
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    }

}

