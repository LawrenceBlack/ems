import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UrlConstant } from '../../../commons/url.constant';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class MangCongViecService {
    private API = UrlConstant.API;

    constructor(private _http: HttpClient) { }

    addMangCongViec(mangCV: Array<string>): Observable<any> {
        return this._http.post<any>(this.API + 'api/test', mangCV ).pipe(
            tap(data => {
                return data;
            }, err => {
                return err;
            })
        );
    }

    getMangCongViecById(idMangCv: string): Observable<any> {
        return this._http.get<any>(this.API + ':id').pipe(
            tap(data => {
                return data;
            },
            (err) => {
                return err;
            })
        );
    }

}
