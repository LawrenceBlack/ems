import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UrlConstant } from '../../../commons/url.constant';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Rx';

// model
import { TopicParent } from '../../../models/TopicParent';

@Injectable()
export class OverviewService {
    private API = UrlConstant.API;
    constructor(private _http: HttpClient) {

    }

    getAllTopicParent(): Observable<TopicParent> {
        return this._http.get<TopicParent>(this.API).pipe(
            tap(
                (data) => {
                    console.log(data);
                },
                (err) => {
                    return err;
                }
            )
        );
    }


}
