import { Component, OnInit, ViewChild } from '@angular/core';
import { TreeNode, Tree, TreeDragDropService } from 'primeng/primeng';
@Component({
    selector: 'app-move-evidence',
    templateUrl: './move-evidence.component.html',
    styleUrls: ['./move-evidence.component.scss'],
    providers: [TreeDragDropService]
})
export class MoveEvidenceComponent implements OnInit {

    files: TreeNode[];
    selectedFile: TreeNode;
    expandingTree: Tree;
    constructor() {

    }
    ngOnInit() {
        this.files = [
            {
                label: 'Mục tiêu minh chứng 1',
                collapsedIcon: 'fa-folder',
                expandedIcon: 'fa-folder-open',
                children: [
                    {
                        label: 'Kế Hoạch Mục Tiêu Minh Chứng 1',
                        collapsedIcon: 'fa-folder',
                        expandedIcon: 'fa-folder-open',
                        children: [
                            {
                                label: 'Nhiêm vụ Mục Tiêu Minh Chứng 1',
                                icon: 'fa-file-o'
                            }
                        ]
                    },
                    {
                        label: 'Folder 2',
                        collapsedIcon: 'fa-folder',
                        expandedIcon: 'fa-folder-open'
                    },
                    {
                        label: 'File 1',
                        icon: 'fa-file-o'
                    }
                ]
            }
        ];
    }
    expandAll(){
        this.files.forEach( node => {
            this.expandRecursive(node, true);
        } );
    }

    collapseAll(){
        this.files.forEach( node => {
            this.expandRecursive(node, false);
        } );
    }
    private expandRecursive(node:TreeNode, isExpand:boolean){
        node.expanded = isExpand;
        if(node.children){
            node.children.forEach( childNode => {
                this.expandRecursive(childNode, isExpand);
            } );
        }
    }
}
