import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { MessageForm } from '../../../../commons/message.constant';

@Component({
    selector: 'form-edit-mang-cong-viec',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class FormEditMangCongViecComponent implements OnInit {
    @Output() onDataEdit: EventEmitter<any> = new EventEmitter<any>();
    @Output() onCancelPopup: EventEmitter<any> = new EventEmitter<any>();
    @Input() modalRef: BsModalRef;
    @Input() objectMangCV: string;
    mess: string = MessageForm.MSG_FIELD_REQUIRED;

    constructor() { }

    public onSubmit(): void {
        let date = 'sending';
        this.onDataEdit.emit(date);
        this.modalRef.hide();
    }
    public closePopup(): void {
        // this.onCancelPopup.emit(null);
        this.modalRef.hide();
    }

    ngOnInit() {
        console.log(this.objectMangCV);
    }

}
