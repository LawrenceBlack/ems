import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { MessageForm } from '../../../../commons/message.constant';

@Component({
    selector: 'form-add-mang-cong-viec',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.scss'],
    providers: []
})
export class FormAddMangCongViecComponent implements OnInit {
    @Output() onDataAdd: EventEmitter<any> = new EventEmitter<any>();
    @Output() onCancelPopup: EventEmitter<any> = new EventEmitter<any>();
    @Input() modalRef: BsModalRef;
    aDeMucs: any[];
    aInputs: Array<any> = [];
    deMuc = { tenDeMuc0: '' };
    mess: string = MessageForm.MSG_FIELD_REQUIRED;
    constructor() { }

    // add an input to form
    appendInput() {
        this.aInputs.push({ tenDeMuc: '' });
    }

    // remove an input
    removeInput(index) {
        this.aInputs.splice(index, 1);
    }

    public onSubmit(f: NgForm): void {
        // map object to array
        let formData = Object.keys(f.value).map((index) => {
            let data = f.value[index];
            return data;
        });
        this.modalRef.hide();
        this.onDataAdd.emit(formData);
    }
    public closePopup(): void {
        // this.onCancelPopup.emit(null);
        this.modalRef.hide();
    }

    ngOnInit() {
    }
}
