import { Component, OnInit, Output, EventEmitter, AfterViewInit } from '@angular/core';

@Component({
    selector: 'form-standard',
    templateUrl: './standard.component.html',
    styleUrls: ['./standard.component.scss']
})
export class FormStandardComponent implements OnInit, AfterViewInit {
    @Output() onDataPicked : EventEmitter<any> = new EventEmitter<any>();
    @Output() onCancelPopup: EventEmitter<any> = new EventEmitter<any>();

    constructor() { }

    public onSubmit(): void {
        let date = 'sending';
        this.onDataPicked.emit(date);
    }
    public closePopup(): void{
        this.onCancelPopup.emit();
    }

    ngOnInit() {
       
    }

    ngAfterViewInit(){
        $('.select-comb').select2({ width: '100%' });
    }

    logout() {
    }
}
