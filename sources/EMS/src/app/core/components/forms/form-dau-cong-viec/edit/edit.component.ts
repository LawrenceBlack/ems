import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'form-edit-dau-cong-viec',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class FormEditDauCongViecComponent implements OnInit {
    @Output() onDataPicked: EventEmitter<any> = new EventEmitter<any>();
    @Output() onCancelPopup: EventEmitter<any> = new EventEmitter<any>();
    constructor() { }

    public onSubmit(): void {
        let date = 'sending';
        this.onDataPicked.emit(date);
    }
    public closePopup(): void {
        this.onCancelPopup.emit();
    }

    ngOnInit() {
    }

    logout() {
    }
}
