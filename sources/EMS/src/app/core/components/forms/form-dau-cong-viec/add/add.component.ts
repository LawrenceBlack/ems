import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'form-add-dau-cong-viec',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.scss']
})
export class FormAddDauCongViecComponent implements OnInit {
    @Output() onDataPicked : EventEmitter<any> = new EventEmitter<any>();
    @Output() onCancelPopup: EventEmitter<any> = new EventEmitter<any>();
    aDeMucs: any[];
	aInputs = [];
	deMuc = {tenDeMuc : ''};
    constructor() { }

    //add an input to form
	appendInput(){
		this.aInputs.push({tenDeMuc: ''});
	}

	//remove an input
	removeInput(index){
		this.aInputs.splice(index, 1);
    }
    
    public onSubmit(): void {
        let date = 'sending';
        this.onDataPicked.emit(date);
    }
    public closePopup(): void{
        this.onCancelPopup.emit();
    }

    ngOnInit() {
    }

    logout() {
    }
}
