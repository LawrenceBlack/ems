import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
    selector: 'app-plan',
    templateUrl: './plan.component.html',
    styleUrls: ['./plan.component.scss']
})
export class PlanComponent implements OnInit {

    modalRef: BsModalRef;

    @Output() onDataPicked : EventEmitter<any> = new EventEmitter<any>();
    @Output() onCancelPopup: EventEmitter<any> = new EventEmitter<any>();
    constructor(private modalService: BsModalService) { }

    public onSubmit(): void {
        let date = 'sending';
        this.onDataPicked.emit(date);
    }
    public closePopup(): void{
        this.onCancelPopup.emit();
    }

    ngOnInit() {
    }

    logout() {
    }
}
