import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-evidence',
    templateUrl: './evidence.component.html',
    styleUrls: ['./evidence.component.scss']
})
export class EvidenceComponent implements OnInit {
    isHovering: boolean = false;
    isSearchAd: boolean = true;
    isSearch: boolean = true;
    @Output() onCancelPopup: EventEmitter<any> = new EventEmitter<any>();
    constructor() { }

    roles = [
        {
            name: 'Tất cả'
        },
        {
            name: 'Ban Giám Hiệu'
        },
        {
            name: 'Trưởng Phòng'
        },
        {
            name: 'Phó Phòng'
        }
    ];

    loaiVanBans = [
        {
            name: 'Thông Báo',
            soHieu: ''
        },
        {
            name: 'Biên Bản'
        },
        {
            name: 'Quyết Định'
        },
        {
            name: 'Tờ Trình'
        },
        {
            name: 'Công Văn'
        },
        {
            name: 'Báo Cáo'
        },
        {
            name: 'Quy Định'
        },
        {
            name: 'Kế Hoạch'
        },
        {
            name: 'Đề Án'
        },
        {
            name: 'Giấy Giới Thiệu'
        },
        {
            name: 'Giấy Mới'
        },
        {
            name: 'Giấy Đi Đường'
        },
        {
            name: 'Giấy Chứng Nhận'
        },
        {
            name: 'Hợp Đồng'
        },
        {
            name: 'Công Điện'
        },
        {
            name: ' Phiếu Gửi'
        }
    ];

    nhomCongViecs = [
        {
            name: 'Quy Trình ISO'
        },
        {
            name: 'Đánh Giá Nội Bộ'
        },
        {
            name: 'Mục Tiêu Chất Lượng'
        },
        {
            name: 'Khảo Sát Chất Lượng Giảng Viên'
        },
        {
            name: 'Đánh Giá AUN Cấp Chương Trình'
        }
    ]
    ngOnInit() {
        $(".select-comb").select2({ width: '100%' });
    }
    showSearch() {
        this.isSearch = !this.isSearch;
        if (!this.isSearch) {
            this.isSearchAd = true;
        }
    }
    showSearchAd() {
        this.isSearchAd = !this.isSearchAd;
    }



    dropzoneState($event: boolean) {
        this.isHovering = $event;
        console.log(this.isHovering);
    }

    handleDrop(fileList: FileList) {
        this.handleFileUpload(fileList);
    }

    handleFileUpload(fileList: FileList) {
        console.log(fileList);
    }

    startUpload(fileList: FileList) {
        this.handleFileUpload(fileList);
    }

    public closePopup(): void {
        this.onCancelPopup.emit();
    }

    logout() {
    }
}
