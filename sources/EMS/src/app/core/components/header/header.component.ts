import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { SystemConstant } from '../../commons/system.constant';
import { UrlConstant } from '../../commons/url.constant';
import { AuthenticationService } from '../../services/authentication.service';
// model
import { SocialUser } from 'angular4-social-login';
var misc: any = {
	navbar_menu_visible: 0,
	active_collapse: true,
	disabled_collapse_init: 0,
};

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
	providers: []
})
export class HeaderComponent implements OnInit {
	private sidebarVisible: boolean;
	private hostName: string;
	private baseUrl: string;
	private nativeElement: Node;
	private toggleButton;
	danhSachThongBaos: any[];
	isAuthen: boolean;
	user: SocialUser;

	constructor(
		private element: ElementRef,
		private router: Router,
		private authenticationService: AuthenticationService
	) {
		this.nativeElement = element.nativeElement;
		this.sidebarVisible = false;
		this.hostName = window.location.host;
		this.baseUrl = this.hostName === UrlConstant.hostName ? '../../../..' : '.';

		// kiểm tra authenticated
		this.isAuthen = this.authenticationService.isAuthenticated();
		if (this.isAuthen) {
			// nếu có thực hiện get thông tin user and image;
			this.authenticationService.getSocialUserGoogle().subscribe(socialUser => {
				this.user = socialUser;
			}, (err) => {
				this.user.name = SystemConstant.USERNAME_DEFAULT;
				this.user.photoUrl = UrlConstant.AVATAR_USER_DEFAULT;
			});
		}
	}

	navigationLink() {
		alert("Go to new page !!!");
	}

	markRead() {
		alert("Function is read");
	}

	ngOnInit() {
		let host = window.location;
		var navbar: HTMLElement = this.element.nativeElement;
		this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];

		if ($('body').hasClass('sidebar-mini')) {
			misc.sidebar_mini_active = true;
		}

		$('#minimizeSidebar').click(function () {
			var $btn = $(this);

			if (misc.sidebar_mini_active == true) {
				$('body').removeClass('sidebar-mini');
				misc.sidebar_mini_active = false;

			} else {
				setTimeout(function () {
					$('body').addClass('sidebar-mini');

					misc.sidebar_mini_active = true;
				}, 300);
			}

			// we simulate the window Resize so the charts will get updated in realtime.
			var simulateWindowResize = setInterval(function () {
				window.dispatchEvent(new Event('resize'));
			}, 180);

			// we stop the simulation of Window Resize after the animations are completed
			setTimeout(function () {
				clearInterval(simulateWindowResize);
			}, 1000);
		});

		this.danhSachThongBaos = [
			{
				imageUrl: this.baseUrl + '/assets/img/faces/face-6.jpg',
				userName: 'Trần Quốc Thiện',
				action: 'Upload 1 file minh chứng lên hệ thống'
			}, {
				imageUrl: this.baseUrl + '/assets/img/faces/face-5.jpg',
				userName: 'Đoàn Văn Thanh Phong',
				action: 'Upload 1 video 4gb lên hệ thống'
			}, {
				imageUrl: this.baseUrl + '/assets/img/faces/face-6.jpg',
				userName: 'Hồ Đặng Hữu Trọng',
				action: 'Vừa comment vào nội dung bạn vừa tải lên'
			}, {
				imageUrl: this.baseUrl + '/assets/img/faces/face-4.jpg',
				userName: 'Trần Quốc Thiện',
				action: 'Upload 1 file minh chứng lên hệ thống'
			}, {
				imageUrl: this.baseUrl + '/assets/img/faces/face-3.jpg',
				userName: 'Trần Quốc Thiện',
				action: 'Upload 1 file minh chứng lên hệ thống'
			}, {
				imageUrl: this.baseUrl + '/assets/img/faces/face-3.jpg',
				userName: 'Trần Quốc Thiện',
				action: 'Upload 1 file minh chứng lên hệ thống'
			}, {
				imageUrl: this.baseUrl + '/assets/img/faces/face-2.jpg',
				userName: 'Trần Quốc Thiện',
				action: 'Upload 1 file minh chứng lên hệ thống'
			}, {
				imageUrl: this.baseUrl + '/assets/img/faces/face-2.jpg',
				userName: 'Trần Quốc Thiện',
				action: 'Upload 1 file minh chứng lên hệ thống'
			}, {
				imageUrl: this.baseUrl + '/assets/img/faces/face-6.jpg',
				userName: 'Trần Quốc Thiện',
				action: 'Upload 1 file minh chứng lên hệ thống'
			}, {
				imageUrl: this.baseUrl + '/assets/img/faces/face-4.jpg',
				userName: 'Trần Quốc Thiện',
				action: 'Upload 1 file minh chứng lên hệ thống'
			}
		];
	}
	isMobileMenu() {
		if ($(window).width() < 991) {
			return false;
		}
		return true;
	}

	sidebarOpen() {
		var toggleButton = this.toggleButton;
		var body = document.getElementsByTagName('body')[0];
		setTimeout(function () {
			toggleButton.classList.add('toggled');
		}, 500);
		body.classList.add('nav-open');
		this.sidebarVisible = true;
	}
	sidebarClose() {
		var body = document.getElementsByTagName('body')[0];
		this.toggleButton.classList.remove('toggled');
		this.sidebarVisible = false;
		body.classList.remove('nav-open');
	}
	sidebarToggle() {
		// var toggleButton = this.toggleButton;
		// var body = document.getElementsByTagName('body')[0];
		if (this.sidebarVisible === false) {
			this.sidebarOpen();
		} else {
			this.sidebarClose();
		}
	}

	onLogout() {
		this.authenticationService.logout();
		// this.router.navigate(['/management/login']);
	}
}
