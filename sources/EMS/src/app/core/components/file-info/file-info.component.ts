import { Component, OnInit } from '@angular/core';
import { ConfirmDialogModule, Message, ConfirmationService } from 'primeng/primeng';
@Component({
    selector: 'app-file-info',
    templateUrl: './file-info.component.html',
    styleUrls: ['./file-info.component.scss'],
    providers: [ConfirmationService]
})
export class FileInfoComponent implements OnInit {
    msgs: Message[] = [];
    idComment: number;
    hostName: string;
    baseUrl: string = '../../../..';
    constructor(private confirmService: ConfirmationService) { 
        this.hostName = window.location.host;
		// this.baseUrl = this.hostName === 'localhost:4200' ? '../../../..' : '.';
    }

    ngOnInit() {
    }

    removeComment() {
        this.confirmService.confirm({
            message: 'Bạn có muốn xóa bình luận này ?',
            header: 'Xác nhận xóa',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.msgs = [{ severity: 'info', summary: 'Thành Công', detail: 'Bạn đồng ý xóa' }];
            },
            reject: () => {
                this.msgs = [{ severity: 'info', summary: 'Hủy Bỏ', detail: 'Đã hủy yêu cầu' }];
            }
        });
    }

    editComment(id: number) {
        this.idComment = id;
    }

    onBlur() {
        this.comments;
        this.idComment = 0;
    }

    onUpdateComment() {
        this.comments;
        this.idComment = 0;
    }

    cancelComment() {
        this.idComment = 0;
    }

    comments = [
        {
            id: 1,
            imageUrl: this.baseUrl + '/assets/img/faces/face-7.jpg',
            user: 'Trần Quốc Thiện',
            content: 'Hệ thống quá tốt',
            child_comment: [
                {
                    id: 2,
                    imageUrl: this.baseUrl + '/assets/img/faces/face-1.jpg',
                    user: 'Đoàn Dự',
                    content: 'Tốt lắm'
                },
                {
                    id: 3,
                    imageUrl: this.baseUrl + '/assets/img/faces/face-1.jpg',
                    user: 'Đoàn Dự',
                    content: 'Tốt lắm'
                }
            ]
        },
        {
            id: 4,
            imageUrl: this.baseUrl + '/assets/img/faces/face-2.jpg',
            user: 'Đoàn Văn Thanh Phong',
            content: 'Hope that helps as a starting point',
            child_comment: [
                {
                    id: 5,
                    imageUrl: this.baseUrl + '/assets/img/faces/face-3.jpg',
                    user: 'Nguyễn Trọng Nghĩa',
                    content: 'Xin phép ad, Cho mình hỏi lớp tư tưởng thầy Tăng tiết 9,10 chiều thứ 6 khi nào nộp tiểu luận vậy mn! Thanks all'
                },
                {
                    id: 6,
                    imageUrl: this.baseUrl + '/assets/img/faces/face-3.jpg',
                    user: 'Nguyễn Trọng Nghĩa',
                    content: 'Hope that helps as a starting point'
                }
            ]
        }
    ]

    logout() {
    }
}
