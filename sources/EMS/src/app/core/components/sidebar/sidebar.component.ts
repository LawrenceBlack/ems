import { Component, OnInit, AfterViewInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { SystemConstant } from '../../commons/system.constant';
import { UrlConstant } from '../../commons/url.constant';
import { AuthenticationService } from '../../services/authentication.service';
import { SocialUser } from 'angular4-social-login';

@Component({
	selector: 'app-sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.scss'],
	providers: []
})
export class SidebarComponent implements OnInit, AfterViewInit {

	modalRef: BsModalRef;
	departments: any[];
	hostName: string;
	baseUrl: string;
	user: SocialUser;
	isAuthen: boolean;
	avatarDefault: string = UrlConstant.AVATAR_USER_DEFAULT;
	logoDefault: string = UrlConstant.LOGO_DEFAULT;
	urlFunctionDuties: string;
	constructor(
		private _router: Router,
		private modalService: BsModalService,
		private authenticationService: AuthenticationService
	) {
		this.hostName = window.location.host;
		this.baseUrl = this.hostName === UrlConstant.hostName ? '../..' : '.';

		// kiểm tra authenticated
		this.isAuthen = this.authenticationService.isAuthenticated();
		if (this.isAuthen) {
			const role = this.authenticationService.checkUtilRole();
			if (role === 1) {
				this.urlFunctionDuties = '/main/client/chuc-nang-nhiem-vu';
			} else {
				if (role === 2) {
					this.urlFunctionDuties = '/main/department/chuc-nang-nhiem-vu/mang-cong-viec';
				} else {
					if (role === 3) {
						this.urlFunctionDuties = '/main/ems/chuc-nang-nhiem-vu';
					}
				}
			}
			// nếu có thực hiện get thông tin user and image;
			this.authenticationService.getSocialUserGoogle().subscribe(socialUser => {
				this.user = socialUser;
			}, (err) => {
				this.user.name = SystemConstant.USERNAME_DEFAULT;
				this.user.photoUrl = UrlConstant.AVATAR_USER_DEFAULT;
			});
		}
	}

	public menuItems: any[];
	public years: any[];

	isNotMobileMenu() {
		if ($(window).width() > 991) {
			return false;
		}
		return true;
	}

	onShowDepartment(template: TemplateRef<any>) {
		this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'gray modal-md' }));
	}

	goTo() {

	}
	ngOnInit() {
		this.departments = [
			{
				name: 'Trung Tâm Công Nghệ Phần Mềm'
			},
			{
				name: 'Trung Tâm Tin Học'
			},
			{
				name: 'Trung Tâm Ngoại Ngữ'
			},
			{
				name: 'Trung Tâm Dịch Vụ Sinh Viên'
			},
			{
				name: 'Hợp Tác Và Đào Tạo Quốc Tế'
			},
			{
				name: 'Dạy Học Số'
			},
			{
				name: 'Trung Tâm Giáo Dục Thể Chất Và Quốc Phòng'
			},
			{
				name: 'Phòng Đào Tạo'
			},
			{
				name: 'Phòng Đào Tạo Không Chính Quy'
			},
			{
				name: 'Phòng Đảm Bảo Chất Lượng'
			},
			{
				name: 'Quan Hệ Quốc Tế'
			},
			{
				name: 'Quan Hệ Doanh Nghiệp'
			},
			{
				name: 'Quản Trị Cơ Sở Vật Chất'
			},
			{
				name: 'Kế Hoạch Và Tài Chính'
			},
			{
				name: 'Thanh Tra - Giáo Dục'
			},
			{
				name: 'Thiết Bị Và Vật Tư'
			},
			{
				name: 'Truyền Thông UTE'
			},
			{
				name: 'Tuyển Sinh Và Công Tác Học Sinh Sinh Viên'
			},
			{
				name: 'Khoa Công Nghệ Thông Tin'
			},
			{
				name: 'Khoa Cơ Khí Động Lực'
			},
			{
				name: 'Khoa Công  Nghệ May Và Thời Trang'
			},

			{
				name: 'Khoa Đào Tạo Chất Lượng Cao'
			},
			{
				name: 'Khoa Điện - Điện Tử'
			},
			{
				name: 'Khoa Lý Luận Chính Trị'
			},
			{
				name: 'Khoa Ngoại Ngữ'
			},
			{
				name: 'Khoa Kinh Tế'
			},
			{
				name: 'Khoa Xây Dựng'
			},
			{
				name: 'Viện Sư Phạm Kỹ Thuật'
			}
		]

		this.years = [
			{
				name: '2016-2017', selected: false
			},
			{
				name: '2017-2018', selected: true
			},
			{
				name: '2018-2019', selected: false
			}
		];
		$('.select-comb').select2({ width: '100%' });
	}
	ngAfterViewInit() {

	}
}
