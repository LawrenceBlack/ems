import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CommentComponent } from '../components/comment/comment.component';
import { DocumentRelatedComponent } from '../components/document-related/document-related.component';
import { FileInfoComponent } from '../components/file-info/file-info.component';
import { PlanComponent } from '../components/plan/plan.component';
import { EvidenceComponent } from '../components/evidence/evidence.component';
import { MoveEvidenceComponent } from '../components/move-evidence/move-evidence.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { Select2Module } from 'ng2-select2';
import { TreeModule, ConfirmDialogModule, GrowlModule, DataTableModule } from 'primeng/primeng';

// form
import { FormAddMangCongViecComponent } from '../components/forms/form-mang-cong-viec/add/add.component';
import { FormEditMangCongViecComponent } from '../components/forms/form-mang-cong-viec/edit/edit.component';
import { FormAddDauCongViecComponent } from '../components/forms/form-dau-cong-viec/add/add.component';
import { FormEditDauCongViecComponent } from '../components/forms/form-dau-cong-viec/edit/edit.component';
import { FormStandardComponent } from '../components/forms/form-standard/standard.component';

// directive
import { DropZoneDirective } from '../directives/dropzone.directive';

// pipe
import { FileSizePipe } from '../pipes/file-size.pipe';
import { FileTypePipe } from '../pipes/file-type.pipe';

@NgModule({
    imports: [
        CommonModule,
        ModalModule.forRoot(),
        BsDropdownModule.forRoot(),
        TooltipModule.forRoot(),
        BsDatepickerModule.forRoot(),
        Select2Module,
        TreeModule,
        ConfirmDialogModule,
        GrowlModule,
        FormsModule,
        DataTableModule
    ],
    declarations: [
        CommentComponent,
        DocumentRelatedComponent,
        FileInfoComponent,
        PlanComponent,
        EvidenceComponent,
        MoveEvidenceComponent,
        DropZoneDirective,
        FormAddMangCongViecComponent,
        FormEditMangCongViecComponent,
        FormAddDauCongViecComponent,
        FormEditDauCongViecComponent,
        FormStandardComponent,
        FileSizePipe,
        FileTypePipe
    ],
    exports: [
        CommentComponent,
        DocumentRelatedComponent,
        FileInfoComponent,
        PlanComponent,
        EvidenceComponent,
        MoveEvidenceComponent,
        FormAddMangCongViecComponent,
        FormEditMangCongViecComponent,
        FormAddDauCongViecComponent,
        FormEditDauCongViecComponent,
        FormStandardComponent,
        ModalModule,
        BsDropdownModule,
        TooltipModule,
        BsDatepickerModule,
        Select2Module,
        DropZoneDirective,
        TreeModule,
        ConfirmDialogModule,
        GrowlModule,
        DataTableModule,
        FileSizePipe,
        FileTypePipe
    ]
})
export class SharedModule { }
