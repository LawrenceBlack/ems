import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FormAddMangCongViecComponent } from '../components/forms/form-mang-cong-viec/add/add.component';
import { FormEditMangCongViecComponent } from '../components/forms/form-mang-cong-viec/edit/edit.component';
import { FormAddDauCongViecComponent } from '../components/forms/form-dau-cong-viec/add/add.component';
import { FormEditDauCongViecComponent } from '../components/forms/form-dau-cong-viec/edit/edit.component';
import { FormStandardComponent } from '../components/forms/form-standard/standard.component';

import { TreeModule, ConfirmDialogModule, GrowlModule, DataTableModule } from 'primeng/primeng';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
// directive

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        BsDropdownModule.forRoot(),
        TreeModule,
        ConfirmDialogModule,
        GrowlModule,
        DataTableModule
    ],
    declarations: [
        // FormAddMangCongViecComponent,
        // FormEditMangCongViecComponent,
        // FormAddDauCongViecComponent,
        // FormEditDauCongViecComponent,
        // FormStandardComponent
    ],
    exports: [
        // FormAddMangCongViecComponent,
        // FormEditMangCongViecComponent,
        // FormAddDauCongViecComponent,
        // FormEditDauCongViecComponent,
        // FormStandardComponent,
    ]
})
export class FormModule { }
