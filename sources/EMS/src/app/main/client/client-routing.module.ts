import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientComponent } from './client.component';
import { ClientOverViewComponent } from './overview/overview.component';
import { ClientTreeComponent } from './chuc-nang-nhiem-vu/tree/tree.component';
import { ClientTableComponent } from './chuc-nang-nhiem-vu/table/table.component';
import { ClientDonViComponent } from './chuc-nang-nhiem-vu/don-vi/don-vi.component';
import { ClientDetailComponent } from './chuc-nang-nhiem-vu/detail/detail.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'client',
        pathMatch: 'full'
    },
    {
        path: '',
        component: ClientComponent,
        children: [
            {
                path: 'client',
                redirectTo: 'overview',
                pathMatch: 'full'
            },
            {
                path: 'overview',
                component: ClientOverViewComponent
            }
            ,
            {
                path: 'chuc-nang-nhiem-vu',
                component: ClientDonViComponent
            },
            {
                path: 'chuc-nang-nhiem-vu/tree',
                component: ClientTreeComponent
            },
            {
                path: 'chuc-nang-nhiem-vu/table',
                component: ClientTableComponent
            },
            {
                path: 'chuc-nang-nhiem-vu/detail',
                component: ClientDetailComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ClientRoutingModule { }
