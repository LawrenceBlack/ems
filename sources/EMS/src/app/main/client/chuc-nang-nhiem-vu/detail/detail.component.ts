import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Router } from '@angular/router';
import { ConfirmDialogModule, Message, SelectItem, ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'client-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss'],
    providers: [ConfirmationService]
})
export class ClientDetailComponent implements OnInit {

    modalRef: BsModalRef;
    msgs: Message[] = [];

    constructor(private modalService: BsModalService, private _router: Router, private confirmService: ConfirmationService) {

    }

    showInfoFile(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'gray modal-lg' }));
    }

    confirm() {
        this.confirmService.confirm({
            message: 'Bạn có muốn xóa mục tiêu này ?',
            header: 'Xác nhận xóa',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.msgs = [{ severity: 'info', summary: 'Thành Công', detail: 'Bạn đồng ý xóa' }];
            },
            reject: () => {
                this.msgs = [{ severity: 'info', summary: 'Hủy Bỏ', detail: 'Đã hủy yêu cầu' }];
            }
        });
    }

    ngOnInit() {
        // $('.select-comb').select2();
    }

}
