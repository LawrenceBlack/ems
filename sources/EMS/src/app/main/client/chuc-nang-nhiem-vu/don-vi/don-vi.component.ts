import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'client-don-vi',
    templateUrl: './don-vi.component.html',
    styleUrls: ['./don-vi.component.scss']
})
export class ClientDonViComponent implements OnInit {
    departments: any[];
    manageFiles: any[];

    constructor() { }

    ngOnInit() {
        this.departments = [
			{
                name: 'Trung Tâm Công Nghệ Phần Mềm',
                user: 'Nguyễn Minh Đạo'
			},
			{
				name: 'Trung Tâm Tin Học'
			},
			{
				name: 'Trung Tâm Ngoại Ngữ'
			},
			{
				name: 'Trung Tâm Dịch Vụ Sinh Viên'
			},
			{
				name: 'Hợp Tác Và Đào Tạo Quốc Tế'
			},
			{
				name: 'Dạy Học Số'
			},
			{
				name: 'Trung Tâm Giáo Dục Thể Chất Và Quốc Phòng'
			},
			{
				name: 'Phòng Đào Tạo'
			},
			{
				name: 'Phòng Đào Tạo Không Chính Quy'
			},
			{
                name: 'Phòng Đảm Bảo Chất Lượng',
                user: 'Đinh Thành Ngân'
			},
			{
				name: 'Quan Hệ Quốc Tế'
			},
			{
				name: 'Quan Hệ Doanh Nghiệp'
			},
			{
				name: 'Quản Trị Cơ Sở Vật Chất'
			},
			{
				name: 'Kế Hoạch Và Tài Chính'
			},
			{
                name: 'Thanh Tra - Giáo Dục',
                user: 'Nguyễn Thanh Sang'
			},
			{
				name: 'Thiết Bị Và Vật Tư'
			},
			{
				name: 'Truyền Thông UTE'
			},
			{
				name: 'Tuyển Sinh Và Công Tác Học Sinh Sinh Viên'
			},
			{
                name: 'Khoa Công Nghệ Thông Tin',
                user: 'Đặng Thanh Dũng'
			},
			{
				name: 'Khoa Cơ Khí Động Lực'
			},
			{
				name: 'Khoa Công  Nghệ May Và Thời Trang'
			},

			{
				name: 'Khoa Đào Tạo Chất Lượng Cao'
			},
			{
				name: 'Khoa Điện - Điện Tử'
			},
			{
				name: 'Khoa Lý Luận Chính Trị'
			},
			{
				name: 'Khoa Ngoại Ngữ'
			},
			{
				name: 'Khoa Kinh Tế'
			},
			{
				name: 'Khoa Xây Dựng'
			},
			{
                name: 'Viện Sư Phạm Kỹ Thuật',
                user: 'Bùi Văn Hồng'
			}
        ];

        this.manageFiles = [
            {
                name: 'Trung Tâm Công Nghệ Phần Mềm',
                files: [
                    {
                        name: 'File_sohoaminhcung.pdf',
                        size: '24 MB',
                        dateUpload: '24/06/2018',
                        userUpload: 'Trần Quốc Thiện',
                        icon: 'fa-file-pdf-o'
                    },
                    {
                        name: 'File_sohoaminhcung.word',
                        size: '2 MB',
                        dateUpload: '04/06/2018',
                        userUpload: 'Nguyễn Trọng Nghĩa',
                        icon: 'fa-file-excel-o'
                    },
                    {
                        name: 'File_sohoaminhcung.word',
                        size: '2 MB',
                        dateUpload: '04/06/2018',
                        userUpload: 'Nguyễn Trọng Nghĩa',
                        icon: 'fa-file-word-o'
                    }
                ]
            },
            {
                name: 'Phòng Tổ Chức Hành Chính',
                files: [
                    {
                        name: 'File_sohoaminhcung.pdf',
                        size: '24 MB',
                        dateUpload: '24/06/2018',
                        userUpload: 'Nguyễn Trọng Bình',
                        icon: 'fa-file-pdf-o'
                    },
                    {
                        name: 'File_sohoaminhcung.word',
                        size: '2 MB',
                        dateUpload: '04/06/2018',
                        userUpload: 'Nguyễn Trọng Nghĩa',
                        icon: 'fa-file-word-o'
                    },
                    {
                        name: 'File_sohoaminhcung.word',
                        size: '2 MB',
                        dateUpload: '04/06/2018',
                        userUpload: 'Nguyễn Trọng Nghĩa',
                        icon: 'fa-file-word-o'
                    }
                ]
            },
            {
                name: 'Phòng Đào Tạo',
                files: [
                    {
                        name: 'File_sohoaminhcung.pdf',
                        size: '24 MB',
                        dateUpload: '24/06/2018',
                        userUpload: 'Nguyễn Thế Bảo',
                        icon: 'fa-file-pdf-o'
                    },
                    {
                        name: 'File_sohoaminhcung.word',
                        size: '2 MB',
                        dateUpload: '04/06/2018',
                        userUpload: 'Nguyễn Trọng Nghĩa',
                        icon: 'fa-file-word-o'
                    },
                    {
                        name: 'File_sohoaminhcung.word',
                        size: '2 MB',
                        dateUpload: '04/06/2018',
                        userUpload: 'Nguyễn Trọng Nghĩa',
                        icon: 'fa-file-word-o'
                    }
                ]
            }
        ];
    }

}
