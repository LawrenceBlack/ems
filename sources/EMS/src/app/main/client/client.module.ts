import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../core/modules/shared.module';
import { ClientRoutingModule } from './client-routing.module';
import { ClientComponent } from './client.component';
import { ClientOverViewComponent } from './overview/overview.component';
import { ClientTreeComponent } from './chuc-nang-nhiem-vu/tree/tree.component';
import { ClientTableComponent } from './chuc-nang-nhiem-vu/table/table.component';
import { ClientDonViComponent } from './chuc-nang-nhiem-vu/don-vi/don-vi.component';
import { ClientDetailComponent } from './chuc-nang-nhiem-vu/detail/detail.component';

@NgModule({
    imports: [
        CommonModule,
        ClientRoutingModule,
        SharedModule
    ],
    declarations: [
        ClientComponent,
        ClientOverViewComponent,
        ClientTreeComponent,
        ClientTableComponent,
        ClientDonViComponent,
        ClientDetailComponent
    ]
})
export class ClientModule { }
