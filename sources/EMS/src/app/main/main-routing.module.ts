import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';
// import { AuthGuard } from '../core/services/authguard/auth.guard';
import { RoleAuthGuard } from '../core/services/authguard/role.guard';

const routes: Routes = [
    {
        path: '',
        redirectTo: '',
        pathMatch: 'full'
    },
    {
        path: '',
        component: MainComponent,
        children: [
            {
                path: 'department',
                loadChildren: './department/department.module#DepartmentModule',
             
            },
            {
                path: 'client',
                loadChildren: './client/client.module#ClientModule',
            
            },
            {
                path: 'ems',
                loadChildren: './dbcl/dbcl.module#DBCLModule',
               
            },
            {
                path: 'page',
                loadChildren: './pages/page.module#PageModule',
                // canActivate: [AuthGuard]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MainRoutingModule { }
