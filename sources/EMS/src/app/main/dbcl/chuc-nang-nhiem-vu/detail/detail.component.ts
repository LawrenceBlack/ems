import { Component, OnInit, Output, EventEmitter } from '@angular/core';

export interface Files {
    name: string;
    size: number;
    type: string;
}

@Component({
    selector: 'dbcl-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class DBCLDetailComponent implements OnInit {
    isHovering: boolean = false;
    isSearchAd: boolean = true;
    isSearch: boolean = true;
    files: Files[];

    @Output() onCancelPopup: EventEmitter<any> = new EventEmitter<any>();
    constructor() { }

    // danh sách hình thức lưu trữ
    hinhThucLuuTrus = [
        {
            name: 'Theo Đầu Công Việc'
        },
        {
            name: 'Theo Trình Tự Thời Gian'
        }
    ];

    // danh sách tệp đính kèm
    tepDinhKems = [
        {
            name: 'CNNV_1.pdf',
            url: '/EMS/MinhChung/ISO/MTCL/CNNV_1.pdf'
        },
        {
            name: 'CNNV_2.pdf',
            url: '/EMS/MinhChung/ISO/MTCL/CNNV_2.pdf'
        },
        {
            name: 'CNNV_3.pdf',
            url: '/EMS/MinhChung/ISO/MTCL/CNNV_3.pdf'
        }
    ];

    showSearch() {
        this.isSearch = !this.isSearch;
        if (!this.isSearch) {
            this.isSearchAd = true;
        }
    }
    showSearchAd() {
        this.isSearchAd = !this.isSearchAd;
    }
    dropzoneState($event: boolean) {
        this.isHovering = $event;
        console.log(this.isHovering);
    }

    handleDrop(fileList: FileList) {
        this.handleFileUpload(fileList);
    }

    handleFileUpload(fileList: FileList) {
        let temp: Files[] = [];
        for (let i = 0; i < fileList.length; i++) {
            let data = {
                name: fileList[i].name,
                size: fileList[i].size,
                type: fileList[i].type,
            };
            temp.push(data);
            this.files = temp;
        }
    }

    startUpload(fileList: FileList) {
        this.handleFileUpload(fileList);
    }

    public closePopup(): void {
        this.onCancelPopup.emit();
    }
    ngOnInit() {
    }

}
