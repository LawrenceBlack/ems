import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
    selector: 'dbcl-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class DBCLTableComponent implements OnInit {

    datas: any[];
    isEdit: boolean = true;
    idEdit: number = 0;

    isEditDauCV: boolean = true;
    idEditDauCV: number = 0;

    modalRef: BsModalRef;
    constructor(private modalService: BsModalService, private _router: Router) { }

    openModalStandard(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(
            template,
            Object.assign({}, { class: 'gray modal-lg' })
        );
    }

    goViewTable() {
        this._router.navigate(['main/ems/chuc-nang-nhiem-vu/table']);
    }

    goViewTree() {
        this._router.navigate(['main/ems/chuc-nang-nhiem-vu/tree']);
    }


    //cancel popup
    onCancelPopup() {
        this.modalRef.hide();
    }

    editMangCongViec(id: number) {
        this.isEdit = false;
        this.idEdit = id;
        console.log(this.idEdit);
    }

    onBlur() {
        this.idEdit = 0;
        this.isEdit = true;
    }

    updateMangCV() {
        this.idEdit = 0;
        this.isEdit = true;
    }

    editDauCongViec(id: number) {
        this.isEditDauCV = false;
        this.idEditDauCV = id;
    }

    onBlurDauCV() {
        this.idEditDauCV = 0;
        this.isEditDauCV = true;
    }

    updateDauCV() {
        this.idEditDauCV = 0;
        this.isEditDauCV = true;
    }

    ngOnInit() {
        this.datas = [
            {
                id: 1,
                name: 'Công tác tổ chức và cán bộ',
                dauCongViecs: [
                    {
                        id: 1,
                        name: 'Chức năng quyền hạn của đơn vị, mô tả trách nhiệm, quyền hạn của viên chức'
                    },
                    {
                        id: 2,
                        name: 'Xây dựng đề án làm việc, định mức biên chế, cơ cấu viên chức'
                    },
                    {
                        id: 3,
                        name: 'Công tác đào tạo, bồi dưỡng'
                    },
                    {
                        id: 4,
                        name: 'Công tác quy hạch cán bộ'
                    },
                    {
                        id: 5,
                        name: 'Công tác tuyển dụng, bổ nhiệm ngạch viên chức'
                    },
                    {
                        id: 6,
                        name: 'Quản lý lao động và hợp đồng làm việc'
                    },
                    {
                        id: 7,
                        name: 'Thực hiện chế độ chính sách'
                    },
                    {
                        id: 8,
                        name: 'Công tác đào tạo bồi dưỡng'
                    }
                ]
            },
            {
                id: 2,
                name: 'Công tác bảo vệ chính trị nội bộ',
                dauCongViecs: [
                    {
                        id: 9,
                        name: 'Thực hiện công tác bảo vệ chính trị nội bộ'
                    }
                ]
            },
            {
                id: 3,
                name: 'Công tác thi đua khen thưởng, kỷ luật',
                dauCongViecs: [
                    {
                        id: 10,
                        name: 'Hồ sơ thi đua khen thưởng'
                    },
                    {
                        id: 11,
                        name: 'Hướng dẫn, tổ chức, thực hiện'
                    },
                    {
                        id: 12,
                        name: 'Hồ sơ kỷ luật'
                    }
                ]
            },
            {
                id: 4,
                name: 'Công tác pháp chế trường học',
                dauCongViecs: [
                    {
                        id: 13,
                        name: 'Thẩm định kiểm tra văn bản nội bộ. Hệ Thống hóa các văn bản pháp quy'
                    },
                    {
                        id: 14,
                        name: 'Tuyên tuyền giáo dục pháp luật'
                    }
                ]
            },
            {
                id: 5,
                name: 'Công tác bảo vệ trị an',
                dauCongViecs: [
                    {
                        id: 15,
                        name: 'Tuần tra, canh gác, kiểm tra'
                    },
                    {
                        id: 16,
                        name: 'PCCC'
                    },
                    {
                        id: 17,
                        name: 'Phối hợp cơ quan công an và chính quyền địa phương về công tác an ninh trật tự của nhà trường'
                    }
                ]
            },
            {
                id: 6,
                name: 'Các công tác khác',
                dauCongViecs: [
                    {
                        id: 18,
                        name: 'Theo sự phân công của cấp trên'
                    }
                ]
            }
        ];
    }
}
