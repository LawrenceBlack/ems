import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DBCLComponent } from './dbcl.component';
import { DBCLOverViewComponent } from './overview/overview.component';
import { DBCLMucTieuChatLuongComponent } from './muc-tieu-chat-luong/muc-tieu-chat-luong.component';
import { DBCLDonViComponent } from './chuc-nang-nhiem-vu/don-vi/don-vi.component';
import { DBCLTreeComponent } from './chuc-nang-nhiem-vu/tree/tree.component';
import { DBCLTableComponent } from './chuc-nang-nhiem-vu/table/table.component';
import { DBCLDetailComponent } from './chuc-nang-nhiem-vu/detail/detail.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'ems',
        pathMatch: 'full'
    },
    {
        path: '',
        component: DBCLComponent,
        children: [
            {
                path: 'ems',
                redirectTo: 'overview',
                pathMatch: 'full'
            },
            {
                path: 'muc-tieu-chat-luong',
                component: DBCLMucTieuChatLuongComponent
            },
            {
                path: 'overview',
                component: DBCLOverViewComponent
            },
            {
                path: 'chuc-nang-nhiem-vu',
                component: DBCLDonViComponent
            },
            {
                path: 'chuc-nang-nhiem-vu/tree',
                component: DBCLTreeComponent
            },
            {
                path: 'chuc-nang-nhiem-vu/table',
                component: DBCLTableComponent
            },
            {
                path: 'chuc-nang-nhiem-vu/detail',
                component: DBCLDetailComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DBCLRoutingModule { }
