import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DBCLRoutingModule } from './dbcl-routing.module';
import { DBCLComponent } from './dbcl.component';
import { DBCLOverViewComponent } from './overview/overview.component';
import { DBCLMucTieuChatLuongComponent } from './muc-tieu-chat-luong/muc-tieu-chat-luong.component';
import { DBCLDonViComponent } from './chuc-nang-nhiem-vu/don-vi/don-vi.component';
import { DBCLTreeComponent } from './chuc-nang-nhiem-vu/tree/tree.component';
import { DBCLTableComponent } from './chuc-nang-nhiem-vu/table/table.component';
import { DBCLDetailComponent } from './chuc-nang-nhiem-vu/detail/detail.component';


//modules
import { SharedModule } from '../../core/modules/shared.module';
import { FormModule } from '../../core/modules/form.module';
@NgModule({
    imports: [
        CommonModule,
        DBCLRoutingModule,
        SharedModule,
        FormModule
    ],
    declarations: [
        DBCLComponent,
        DBCLOverViewComponent,
        DBCLMucTieuChatLuongComponent,
        DBCLDonViComponent,
        DBCLTableComponent,
        DBCLTreeComponent,
        DBCLDetailComponent
    ]
})
export class DBCLModule { }
