import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { SharedModule } from '../core/modules/shared.module';

import { HeaderComponent } from '../core/components/header/header.component';
import { FooterComponent } from '../core/components/footer/footer.component';
import { SidebarComponent } from '../core/components/sidebar/sidebar.component';

// auth
import { RoleAuthGuard } from '../core/services/authguard/role.guard';
import { AuthInterceptor } from '../core/services/authguard/authInterceptor.service';
// interceptor
// import { httpInterceptorProviders } from '../core/services/authguard/interceptor.index';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        SharedModule,
        MainRoutingModule
    ],
    declarations: [
        MainComponent,
        HeaderComponent,
        FooterComponent,
        SidebarComponent
    ],
    providers: [
        RoleAuthGuard, { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
    ],
    exports: [
        RouterModule,
        HeaderComponent,
        FooterComponent,
        SidebarComponent
    ]
})
export class MainModule { }
