import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
    selector: 'department-tree',
    templateUrl: './tree.component.html',
    styleUrls: ['./tree.component.scss']
})
export class DepartmentTreeComponent implements OnInit {

    modalRef: BsModalRef;
    constructor(private modalService: BsModalService, private _router: Router) { }

    openModalAdd(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(
            template,
            Object.assign({}, { class: 'gray modal-md' })
        );
    }

    openModalEdit(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(
            template,
            Object.assign({}, { class: 'gray modal-lg' })
        );
    }

    goViewTable() {
        this._router.navigate(['main/department/chuc-nang-nhiem-vu/table']);
    }

    goViewTree() {
        this._router.navigate(['main/department/chuc-nang-nhiem-vu/tree']);
    }

    onCancelPopup() {
        this.modalRef.hide();
    }

    ngOnInit() {
    }

}
