import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ConfirmDialogModule, Message, SelectItem, ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'department-dau-cong-viec',
    templateUrl: './dau-cong-viec.component.html',
    styleUrls: ['./dau-cong-viec.component.scss'],
    providers: [ConfirmationService]
})
export class DepartmentDauCongViecComponent implements OnInit {

    modalRef: BsModalRef;
    msgs: Message[] = [];
    dauCongViecs: any[];

    constructor(
        private confirmService: ConfirmationService,
        private modalService: BsModalService,
        private _router: Router) { }

    openModalAdd(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(
            template,
            Object.assign({}, { class: 'gray modal-md' })
        );
    }

    openModalEdit(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(
            template,
            Object.assign({}, { class: 'gray modal-md' })
        );
    }

    goViewTable() {
        this._router.navigate(['main/department/chuc-nang-nhiem-vu/table']);
    }

    goViewTree() {
        this._router.navigate(['main/department/chuc-nang-nhiem-vu/tree']);
    }

    onCancelPopup() {
        this.modalRef.hide();
    }

    confirm() {
        this.confirmService.confirm({
            message: 'Bạn có muốn xóa mảng công việc này ?',
            header: 'Xác nhận xóa',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.msgs = [{ severity: 'info', summary: 'Thành Công', detail: 'Bạn đồng ý xóa' }];
            },
            reject: () => {
                this.msgs = [{ severity: 'info', summary: 'Hủy Bỏ', detail: 'Đã hủy yêu cầu' }];
            }
        });
    }

    ngOnInit() {
        this.dauCongViecs = [
            {
                name: 'Khảo sát SV về hoạt động giảng dạy của SV'
            },
            {
                name: 'Khảo sát sự hài lòng của SV về chất lượng phục vụ của nhà trường'
            },
            {
                name: 'Khảo sát sinh viên tốt nghiệp'
            },
            {
                name: 'Khảo sát cựu sinh viên'
            },
            {
                name: 'Khảo sát CBVC về môi trường làm việc'
            },
            {
                name: 'Hỗ trợ các đơn vị thực hiện khảo sát'
            }
        ];
    }
}
