import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
    selector: 'department-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class DepartmentTableComponent implements OnInit {

    datas: any[];

    modalRef: BsModalRef;
    constructor(private modalService: BsModalService, private _router: Router) { }

    openModalAdd(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(
            template,
            Object.assign({}, { class: 'gray modal-md' })
        );
    }

    openModalEdit(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(
            template,
            Object.assign({}, { class: 'gray modal-lg' })
        );
    }

    goViewTable() {
        this._router.navigate(['main/department/chuc-nang-nhiem-vu/table']);
    }

    goViewTree() {
        this._router.navigate(['main/department/chuc-nang-nhiem-vu/tree']);
    }

    onCancelPopup() {
        this.modalRef.hide();
    }

    ngOnInit() {
        this.datas = [
            {
                name: 'Công tác tổ chức và cán bộ',
                dauCongViecs: [
                    {
                        name: 'Chức năng quyền hạn của đơn vị, mô tả trách nhiệm, quyền hạn của viên chức'
                    },
                    {
                        name: 'Xây dựng đề án làm việc, định mức biên chế, cơ cấu viên chức'
                    },
                    {
                        name: 'Công tác đào tạo, bồi dưỡng'
                    },
                    {
                        name: 'Công tác quy hạch cán bộ'
                    },
                    {
                        name: 'Công tác tuyển dụng, bổ nhiệm ngạch viên chức'
                    },
                    {
                        name: 'Quản lý lao động và hợp đồng làm việc'
                    },
                    {
                        name: 'Thực hiện chế độ chính sách'
                    },
                    {
                        name: 'Công tác đào tạo bồi dưỡng'
                    }
                ]
            },
            {
                name: 'Công tác bảo vệ chính trị nội bộ',
                dauCongViecs: [
                    {
                        name: 'Thực hiện công tác bảo vệ chính trị nội bộ'
                    }
                ]
            },
            {
                name: 'Công tác thi đua khen thưởng, kỷ luật',
                dauCongViecs: [
                    {
                        name: 'Hồ sơ thi đua khen thưởng'
                    },
                    {
                        name: 'Hướng dẫn, tổ chức, thực hiện'
                    },
                    {
                        name: 'Hồ sơ kỷ luật'
                    }
                ]
            },
            {
                name: 'Công tác pháp chế trường học',
                dauCongViecs: [
                    {
                        name: 'Thẩm định kiểm tra văn bản nội bộ. Hệ Thống hóa các văn bản pháp quy'
                    },
                    {
                        name: 'Tuyên tuyền giáo dục pháp luật'
                    }
                ]
            },
            {
                name: 'Công tác bảo vệ trị an',
                dauCongViecs: [
                    {
                        name: 'Tuần tra, canh gác, kiểm tra'
                    },
                    {
                        name: 'PCCC'
                    },
                    {
                        name: 'Phối hợp cơ quan công an và chính quyền địa phương về công tác an ninh trật tự của nhà trường'
                    }
                ]
            },
            {
                name: 'Các công tác khác',
                dauCongViecs: [
                    {
                        name: 'Theo sự phân công của cấp trên'
                    }
                ]
            }
        ];


    }

}
