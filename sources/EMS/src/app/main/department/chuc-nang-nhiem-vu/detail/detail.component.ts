import { Component, OnInit, Output, EventEmitter, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ConfirmDialogModule, Message, SelectItem, ConfirmationService } from 'primeng/primeng';

export interface Files {
    name: string;
    size: number;
    type: string;
}

@Component({
    selector: 'department-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss'],
    providers: [ConfirmationService]
})
export class DepartmentDetailComponent implements OnInit {

    isHovering: boolean = false;
    isSearchAd: boolean = true;
    isSearch: boolean = true;
    modalRef: BsModalRef;
    msgs: Message[] = [];
    files: Files[];

    @Output() onCancelPopup: EventEmitter<any> = new EventEmitter<any>();

    constructor(private modalService: BsModalService, private confirmService: ConfirmationService) { }

    // danh sách hình thức lưu trữ
    hinhThucLuuTrus = [
        {
            name: 'Theo Đầu Công Việc'
        },
        {
            name: 'Theo Trình Tự Thời Gian'
        }
    ];

    // danh sách tệp đính kèm
    tepDinhKems = [
        {
            name: 'CNNV_1.pdf',
            url: '/EMS/MinhChung/ISO/MTCL/CNNV_1.pdf'
        },
        {
            name: 'CNNV_2.pdf',
            url: '/EMS/MinhChung/ISO/MTCL/CNNV_2.pdf'
        },
        {
            name: 'CNNV_3.pdf',
            url: '/EMS/MinhChung/ISO/MTCL/CNNV_3.pdf'
        }
    ];

    openDialog(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'gray modal-lg' }));
    }

    openModalInfoFile(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'gray modal-lg' }));
    }

    showSearch() {
        this.isSearch = !this.isSearch;
        if (!this.isSearch) {
            this.isSearchAd = true;
        }
    }
    showSearchAd() {
        this.isSearchAd = !this.isSearchAd;
    }

    removeFile() {
        this.confirmService.confirm({
            message: 'Bạn có muốn xóa minh chứng này ?',
            header: 'Xác nhận xóa',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.msgs = [
                    {
                        severity: 'info', summary: 'Thành Công',
                        detail: 'Bạn đồng ý xóa'
                    }
                ];
            },
            reject: () => {
                this.msgs = [
                    {
                        severity: 'info',
                        summary: 'Hủy Bỏ',
                        detail: 'Đã hủy yêu cầu'
                    }
                ];
            }
        });
    }

    removeItem() {
        this.confirmService.confirm({
            message: 'Bạn có muốn xóa mục này ?',
            header: 'Xác nhận xóa',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.msgs = [{ severity: 'info', summary: 'Thành Công', detail: 'Bạn đồng ý xóa' }];
            },
            reject: () => {
                this.msgs = [{ severity: 'info', summary: 'Hủy Bỏ', detail: 'Đã hủy yêu cầu' }];
            }
        });
    }

    dropzoneState($event: boolean) {
        this.isHovering = $event;
        console.log(this.isHovering);
    }

    handleDrop(fileList: FileList) {
        this.handleFileUpload(fileList);
    }

    handleFileUpload(fileList: FileList) {
        let temp: Files[] = [];
        for (let i = 0; i < fileList.length; i++) {
            let data = {
                name: fileList[i].name,
                size: fileList[i].size,
                type: fileList[i].type,
            };
            temp.push(data);
            this.files = temp;
        }
    }

    startUpload(fileList: FileList) {
        this.handleFileUpload(fileList);
    }

    public closePopup(): void {
        this.onCancelPopup.emit();
    }

    ngOnInit() {
        $('.select-comb').select2({ width: '100%' });
    }
}
