import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ConfirmDialogModule, Message, SelectItem, ConfirmationService } from 'primeng/primeng';
import { MangCongViecService } from '../../../../core/services/department/chuc-nang-nhiem-vu/mangcongviec.service';

@Component({
    selector: 'department-mang-cong-viec',
    templateUrl: './mang-cong-viec.component.html',
    styleUrls: ['./mang-cong-viec.component.scss'],
    providers: [ConfirmationService, MangCongViecService]
})
export class DepartmentMangCongViecComponent implements OnInit {

    modalRef: BsModalRef;
    msgs: Message[] = [];
    datas: any[];
    objectMangCv: any;

    constructor(
        private confirmService: ConfirmationService,
        private modalService: BsModalService,
        private mangCVService: MangCongViecService,
        private _router: Router) { }

    openModalAdd(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(
            template,
            Object.assign({}, { class: 'gray modal-md' })
        );
    }

    openModalEdit(template: TemplateRef<any>, id: string) {

        this.mangCVService.getMangCongViecById(id).subscribe(
            (result) => {
                this.modalRef = this.modalService.show(
                    template,
                    Object.assign({}, { class: 'gray modal-md' })
                );
                this.objectMangCv = result;
                console.log(this.objectMangCv);
            },
            (err) => {
                this.modalRef = this.modalService.show(
                    template,
                    Object.assign({}, { class: 'gray modal-md' })
                );
                this.objectMangCv = { name: 'test'};
                return;
            }
        );
    }

    goViewTable() {
        this._router.navigate(['main/department/chuc-nang-nhiem-vu/table']);
    }

    goViewTree() {
        this._router.navigate(['main/department/chuc-nang-nhiem-vu/tree']);
    }

    onCancelPopup(event) {
        this.modalRef.hide();
    }

    onDataAdd(event) {
        if (!event) {
            // show notify
            return;
        }
        // this.modalRef.hide();
        this.mangCVService.addMangCongViec(event).subscribe(
            (result) => {
                this.datas = this.datas.concat(result); // ghep 2 mang gia tri
            },
            (err) => {
                console.log(err);
            }
        );
    }
    onDataEdit(event) {
        console.log(event);
    }

    confirm() {
        this.confirmService.confirm({
            message: 'Bạn có muốn xóa mảng công việc này ?',
            header: 'Xác nhận xóa',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.msgs = [{ severity: 'info', summary: 'Thành Công', detail: 'Bạn đồng ý xóa' }];
            },
            reject: () => {
                this.msgs = [{ severity: 'info', summary: 'Hủy Bỏ', detail: 'Đã hủy yêu cầu' }];
            }
        });
    }

    ngOnInit() {
        this.datas = [
            {
                _id: 1,
                name: 'Quản lý chất lượng theo hệ thống ISO'
            },
            {
                _id: 2,
                name: 'Khảo sát'
            },
            {
                _id: 3,
                name: 'Đánh Giá - Kiểm Định'
            }
        ];
    }
}
