import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepartmentRoutingModule } from './department-routing.module';
import { DepartmentComponent } from './department.component';
import { DepartmentOverViewComponent } from './overview/overview.component';
import { DepartmentTreeComponent } from './chuc-nang-nhiem-vu/tree/tree.component';
import { DepartmentTableComponent } from './chuc-nang-nhiem-vu/table/table.component';
import { DepartmentMangCongViecComponent } from './chuc-nang-nhiem-vu/mang-cong-viec/mang-cong-viec.component';
import { DepartmentMucTieuChatLuongComponent } from './muc-tieu-chat-luong/muc-tieu-chat-luong.component';
import { DepartmentDauCongViecComponent } from './chuc-nang-nhiem-vu/dau-cong-viec/dau-cong-viec.component';
import { DepartmentDetailComponent } from './chuc-nang-nhiem-vu/detail/detail.component';

import { SharedModule } from '../../core/modules/shared.module';
import { FormModule } from '../../core/modules/form.module';

@NgModule({
    imports: [
        CommonModule,
        DepartmentRoutingModule,
        SharedModule,
        FormModule
    ],
    declarations: [
        DepartmentComponent,
        DepartmentOverViewComponent,
        DepartmentMucTieuChatLuongComponent,
        DepartmentTreeComponent,
        DepartmentTableComponent,
        DepartmentMangCongViecComponent,
        DepartmentDauCongViecComponent,
        DepartmentDetailComponent
    ]
})
export class DepartmentModule { }
