import { Component, Directive, OnInit, TemplateRef, AfterViewInit, Input } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ConfirmDialogModule, Message, SelectItem, ConfirmationService } from 'primeng/primeng';
import { Router } from '@angular/router';
import { OverviewService } from '../../../core/services/department/overview/overview.service';
import * as screenfull from 'screenfull';

@Component({
    selector: 'department-overview',
    templateUrl: './overview.component.html',
    styleUrls: ['./overview.component.scss'],
    providers: [ConfirmationService, OverviewService]
})

// @Directive({
//     selector: '[toggleFullscreen]'
// })

export class DepartmentOverViewComponent implements OnInit, AfterViewInit {

    modalRef: BsModalRef;
    msgs: Message[] = [];

    datas: any[];
    colMenus = [
        {
            col: 'Chọn Tất Cả',
            check: false
        },
        {
            col: 'STT',
            check: false
        },
        {
            col: 'Mục Tiêu Cụ Thể',
            check: true
        },
        {
            col: 'KPIs',
            check: true
        },
        {
            col: 'Kế Hoạch Thực Hiện',
            check: false
        },
        {
            col: 'Bắt Đầu',
            check: false
        },
        {
            col: 'Kết Thúc',
            check: false
        },
        {
            col: 'Chỉ Đạo',
            check: true
        },
        {
            col: 'Người Thực Hiện',
            check: true
        },
        {
            col: 'Minh Chứng',
            check: false
        }
    ];
    indexCollapse: number;
    constructor(
        private confirmService: ConfirmationService,
        private modalService: BsModalService,
        private overService: OverviewService,
        private _router: Router) { }

    onInfoFile(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'gray modal-lg' }));
    }

    onShowComment(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'gray modal-lg' }));
    }

    onDanhGiaNoiBo(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'gray modal-lg' }));
    }

    goMucTieuChatLuong() {
        const link = '/main/department/muc-tieu-chat-luong';
        this._router.navigate([link]);
    }

    confirm() {
        this.confirmService.confirm({
            message: 'Bạn có muốn xóa mục tiêu này ?',
            header: 'Xác nhận xóa',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.msgs = [{ severity: 'info', summary: 'Thành Công', detail: 'Bạn đồng ý xóa' }];
            },
            reject: () => {
                this.msgs = [{ severity: 'info', summary: 'Hủy Bỏ', detail: 'Đã hủy yêu cầu' }];
            }
        });
    }


    onReload() {
        this.datas = [
            {
                roman: 'I',
                title: 'Công Tác Quản Trị Đại Học',
                childrents: [
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    }
                ]
            },
            {
                roman: 'II',
                title: 'Công Tác Cơ Cấu Tổ Chức và Nhân Sự',
                childrents: [
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    }
                ]
            }
        ];
    }

    onFullScreen() {
        console.log('fullscreen');
        const el = document.getElementById('fullScreen');
        if (screenfull.enabled) {
            screenfull.request(el);
        }
        // document.getElementById('button').addEventListener('click', () => {

        // });
    }

    onCollapseShow() {
        $('.arrCollapse').parents('tbody').find('tr:not(:first-child)').show();
    }

    onCollapseHide() {
        $('.arrCollapse').parents('tbody').find('tr:not(:first-child)').hide();
    }

    ngOnInit() {
        this.datas = [
            {
                roman: 'I',
                title: 'Công Tác Quản Trị Đại Học',
                childrents: [
                    {
                        title: 'Rà soát hệ thống văn bản quản lý - quản trị hiện hành theo lĩnh vực hoạt động của đơn vị.',
                        kpis: 'Danh mục hệ thống văn bản quản lý - quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Thực hiện các hoạt động theo kế hoạch của Nhà trường',
                                startTime: '09/2017',
                                endTime: '08/2018'
                            }
                        ],
                        boss: 'Nguyễn Minh Đạo',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'CBVC đơn vị'
                    },
                    {
                        title: 'Triển khai đánh giá hiệu quả hoạt động của đơn vị theo KPIs.',
                        kpis: 'Trung tâm mới hoạt động 06/2017. Báo cáo mục tiêu KPIs của đơn vị từ 06/2017 đến 12/2017',
                        plans: [
                            {
                                title: 'Thực hiện các hoạt động theo kế hoạch của Nhà trường',
                                startTime: '09/2017',
                                endTime: '08/2018'
                            }
                        ],
                        boss: 'Nguyễn Minh Đạo',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'CBVC đơn vị'
                    },
                    {
                        title: 'Tham gia Lễ kỷ niệm 55 năm ngày thành lập trường theo sư phân công của Nhà trường.',
                        kpis: '100% CBVC tham gia',
                        plans: [
                            {
                                title: 'Thực hiện các hoạt động theo kế hoạch của Nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            }
                        ],
                        boss: 'Nguyễn Minh Đạo',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'CBVC đơn vị'
                    }
                ]
            },
            {
                roman: 'II',
                title: 'CÔNG TÁC CƠ CẤU TỔ CHỨC VÀ NHÂN SỰ',
                childrents: [
                    {
                        title: 'Tham gia triển khai đổi mới môi trường làm việc ',
                        kpis: 'Triển khai thực hiện mô hình của Trường: Công sở hiện đại',
                        plans: [
                            {
                                title: 'Thực hiện các hoạt động theo kế hoạch của Nhà trường',
                                startTime: '09/2017',
                                endTime: '08/2018'
                            }
                        ],
                        boss: 'Nguyễn Minh Đạo',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'CBVC đơn vị'
                    },
                    {
                        title: 'Xây dựng chính sách thu hút người giỏi và chính sách dưỡng tài, có chính sách tuyển dụng bồi dưỡng sinh viên giỏi có tiềm năng của trường.',
                        kpis: 'Triển khai theo Bản Quy định về chính sách thu hút người giỏi và chính sách dưỡng tài trong tuyển dụng của nhà trường.',
                        plans: [
                            {
                                title: 'Thực hiện các hoạt động theo kế hoạch của Nhà trường',
                                startTime: '09/2017',
                                endTime: '08/2018'
                            }
                        ],
                        boss: 'Nguyễn Minh Đạo',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'CBVC đơn vị'
                    }
                ]
            },
            {
                roman: 'III',
                title: 'CÔNG TÁC ĐẢM BẢO CHẤT LƯỢNG',
                childrents: [
                    {
                        title: 'Triển khai các giải pháp để khắc phục các điểm còn tồn tại sau Kiểm định chất lượng nhà trường và 04 CTĐT theo AUN - QA.',
                        kpis: 'Báo cáo các giải pháp khắc phục các điểm còn tồn tại sau Kiểm định chất lượng nhà trường và 04 CTĐT theo AUN – QA',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            },
                            {
                                title: 'Tổ chức các hoạt động',
                                startTime: '11/2017',
                                endTime: '02/2018'
                            },
                            {
                                title: 'Bản kế hoạch tổng thể để khắc phục các điểm còn tồn tại sau Kiểm định chất lượng nhà trường và 04 CTĐT theo AUN - QA được phê duyệt',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'Đinh Thành Ngân',
                        department: 'Phòng Đảm Bảo Chất Lượng',
                        deparment_im: 'CBVC đơn vị'
                    },
                    {
                        title: 'Hoàn thành đánh giá theo tiêu chuẩn AUN 04 CTĐT (tháng 11/2017).',
                        kpis: '04 CTĐT được đánh giá:',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            },
                            {
                                title: 'Tổ chức các hoạt động',
                                startTime: '11/2017',
                                endTime: '11/2017'
                            },
                            {
                                title: 'Tổng kết và đánh giá kết quả các hoạt động',
                                startTime: '12/2017',
                                endTime: '03/2018'
                            }
                        ],
                        boss: 'Phan Thị Thu Thủy',
                        department: 'Phòng Đảm Bảo Chất Lượng',
                        deparment_im: 'CBVC đơn vị'
                    },
                    {
                        title: 'Công tác chuẩn bị cho 03 CTĐT đánh giá AUN (11/2018).',
                        kpis: 'Bản SAR, minh chứng và công tác chuẩn bị CSVC version 3',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '09/2017'
                            },
                            {
                                title: 'Tổ chức các hoạt động',
                                startTime: '10/2017',
                                endTime: '07/2018'
                            },
                            {
                                title: 'Báo cáo kế quả công tác chuẩn bị cho 03 CTĐT đánh giá AUN (11/2018)',
                                startTime: '07/2018',
                                endTime: '08/2018'
                            }
                        ],
                        boss: 'Phan Thị Thu Thủy',
                        department: 'Phòng Đảm Bảo Chất Lượng',
                        deparment_im: 'CBVC đơn vị'
                    },
                    {
                        title: 'Đánh giá sơ bộ mức độ đáp ứng của Trường với bộ tiêu chuẩn đánh giá cấp trường của AUN, đề xuất các đơn vị cải tiến các mảng công việc cho phù hợp với bộ tiêu chuẩn đánh giá cấp trường của AUN.',
                        kpis: 'Quy chế đảm bảo chất lượng bên trong; Sổ tay đảm bảo chất lượng (sửa đổi, bổ sung)',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '02/2018',
                                endTime: '02/2018'
                            },
                            {
                                title: 'Tổ chức các hoạt động',
                                startTime: '03/2018',
                                endTime: '07/2018'
                            },
                            {
                                title: 'Ban hành Quy chế đảm bảo chất lượng bên trong; Sổ tay đảm bảo chất lượng (sửa đổi, bổ sung)',
                                startTime: '07/2018',
                                endTime: '08/2018'
                            }
                        ],
                        boss: 'Phan Thị Thu Thủy',
                        department: 'Phòng Đảm Bảo Chất Lượng',
                        deparment_im: 'CBVC đơn vị'
                    },
                    {
                        title: 'Bước đầu triển khai Kế hoạch chiến lược của trường',
                        kpis: 'Kế hoạch chiến lược ĐBCL của Nhà trường',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '02/2018',
                                endTime: '02/2018'
                            },
                            {
                                title: 'Tổ chức các hoạt động',
                                startTime: '03/2018',
                                endTime: '07/2018'
                            },
                            {
                                title: 'Ban Hành Kế hoạch chiến lược ĐBCL của Nhà trường',
                                startTime: '07/2018',
                                endTime: '08/2018'
                            }
                        ],
                        boss: 'Đinh Thành Ngân',
                        department: 'Phòng Đảm Bảo Chất Lượng',
                        deparment_im: 'CBVC đơn vị'
                    }
                ]
            },
            {
                roman: 'IV',
                title: 'CÔNG TÁC TÀI CHÍNH VÀ CƠ SỞ VẬT CHẤT',
                childrents: [
                    {
                        title: 'Tham gia thực hiện theo kế hoạch chiến lược về hệ thống CNTT-TT giai đoạn 2017-2022 theo định hướng tự chủ, có hệ thống CSDL và các module ứng dụng thông minh theo hướng ứng dụng big data, đảm bảo sự ổn định, an toàn,an ninh mạng, kết nối băng thông rộng, đáp ứng theo thời gian thực',
                        kpis: '- Các văn bản về hệ thống CNTT-TT của đơn vị - Hệ thống số hóa minh chứng phục vụ hoạt động ĐBCL của Nhà trường',
                        plans: [
                            {
                                title: '- Triển khai thực hiện theo kế hoạch về hệ thống CNTT của Trường - Xây dựng phần mềm hệ thống số hóa minh chứng phục vụ hoạt động ĐBCL của Nhà trường',
                                startTime: '09/2017',
                                endTime: '08/2018'
                            }
                        ],
                        boss: 'Phan Thị Thu Thủy',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'CBVC đơn vị'
                    }
                ]
            },
        ];
        // this.overService.getAllTopicParent().subscribe(
        //     (topic) => {
        //         console.log(topic);
        //     },
        //     (err) => {
        //         console.log(err);
        //         return;
        //     }
        // );
    }

    ngAfterViewInit() {

    }
    getIndex(id: string) {
        // $(document).on('click', `.index_${id}`, function (e) {
        //     e.preventDefault();
        //     // $(this).parents('tbody').find('tr:not(:first-child)').toggle();
        //     $(this).parents('tbody').find(`tr.row_${id}`).toggle();
        // });

        // animation collapse js
        $(`.index_${id}`).parents('tbody').find(`tr.row_${id}`).toggle();

    }

}
