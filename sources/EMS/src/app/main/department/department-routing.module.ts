import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DepartmentComponent } from './department.component';
import { DepartmentOverViewComponent } from './overview/overview.component';
import { DepartmentMucTieuChatLuongComponent } from './muc-tieu-chat-luong/muc-tieu-chat-luong.component';
import { DepartmentTreeComponent } from './chuc-nang-nhiem-vu/tree/tree.component';
import { DepartmentTableComponent } from './chuc-nang-nhiem-vu/table/table.component';
import { DepartmentMangCongViecComponent } from './chuc-nang-nhiem-vu/mang-cong-viec/mang-cong-viec.component';
import { DepartmentDauCongViecComponent } from './chuc-nang-nhiem-vu/dau-cong-viec/dau-cong-viec.component';
import { DepartmentDetailComponent } from './chuc-nang-nhiem-vu/detail/detail.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'department',
        pathMatch: 'full'
    },
    {
        path: '',
        component: DepartmentComponent,
        children: [
            {
                path: 'department',
                redirectTo: 'overview',
                pathMatch: 'full'
            },
            {
                path: 'muc-tieu-chat-luong',
                component: DepartmentMucTieuChatLuongComponent
            },
            {
                path: 'overview',
                component: DepartmentOverViewComponent
            },
            {
                path: 'chuc-nang-nhiem-vu/tree',
                component: DepartmentTreeComponent
            },
            {
                path: 'chuc-nang-nhiem-vu/table',
                component: DepartmentTableComponent
            },
            {
                path: 'chuc-nang-nhiem-vu/mang-cong-viec',
                component: DepartmentMangCongViecComponent
            },
            {
                path: 'chuc-nang-nhiem-vu/mang-cong-viec/dau-cong-viec',
                component: DepartmentDauCongViecComponent
            },
            {
                path: 'chuc-nang-nhiem-vu/mang-cong-viec/dau-cong-viec/chi-tiet',
                component: DepartmentDetailComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DepartmentRoutingModule { }
