import { Component, OnInit, TemplateRef, AfterViewInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Select2OptionData } from 'ng2-select2';
import { ConfirmDialogModule, Message, ConfirmationService } from 'primeng/primeng';
import * as screenfull from 'screenfull';
@Component({
    selector: 'department-muc-tieu-chat-luong',
    templateUrl: './muc-tieu-chat-luong.component.html',
    styleUrls: ['./muc-tieu-chat-luong.component.scss'],
    providers: [ConfirmationService]
})

export class DepartmentMucTieuChatLuongComponent implements OnInit, AfterViewInit {

    modalRef: BsModalRef;
    msgs: Message[] = [];
    checkCard = false;
    checkCardList = false;
    public exampleData: Array<Select2OptionData>;
    public keHoachs: any[];
    datas: any[];
    colMenus = [
        {
            col: 'STT',
            check: false
        },
        {
            col: 'Kế Hoạch Thực Hiện',
            check: false
        },
        {
            col: 'Bắt Đầu',
            check: false
        },
        {
            col: 'Kết Thúc',
            check: false
        },
        {
            col: 'Minh Chứng',
            check: false
        }
    ];
    constructor(private modalService: BsModalService, private confirmService: ConfirmationService) { }

    onInfoFile(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'gray modal-lg' }));
    }

    onAttachFile(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'gray modal-lg' }));
    }

    openEvidence() {
        this.checkCard = true;
        $('.select-comb').select2({ width: '100%' });
    }
    showComment(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'gray modal-lg' }));
    }

    onMoveFile(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'gray modal-lg' }));
    }

    onPlan(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'gray modal-lg' }));
    }

    openCheckCardList() {
        this.checkCardList = true;
    }

    removePlan() {
        this.confirmService.confirm({
            message: 'Bạn có muốn xóa Kế hoạch thực hiện này ?',
            header: 'Xác nhận xóa',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.msgs = [{ severity: 'info', summary: 'Thành Công', detail: 'Bạn đồng ý xóa' }];
            },
            reject: () => {
                this.msgs = [{ severity: 'info', summary: 'Hủy Bỏ', detail: 'Đã hủy yêu cầu' }];
            }
        });
    }

    removeDes() {
        this.confirmService.confirm({
            message: 'Bạn có muốn xóa Mục tiêu thực hiện này ?',
            header: 'Xác nhận xóa',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.msgs = [{ severity: 'info', summary: 'Thành Công', detail: 'Bạn đồng ý xóa' }];
            },
            reject: () => {
                this.msgs = [{ severity: 'info', summary: 'Hủy Bỏ', detail: 'Đã hủy yêu cầu' }];
            }
        });
    }

    removeFile() {
        this.confirmService.confirm({
            message: 'Bạn có muốn xóa minh chứng này khỏi hệ thống ?',
            header: 'Xác nhận xóa',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.msgs = [{ severity: 'info', summary: 'Thành Công', detail: 'Bạn đồng ý xóa' }];
            },
            reject: () => {
                this.msgs = [{ severity: 'info', summary: 'Hủy Bỏ', detail: 'Đã hủy yêu cầu' }];
            }
        });
    }

    onReload() {
        this.datas = [
            {
                roman: 'I',
                title: 'Công Tác Quản Trị Đại Học',
                childrents: [
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    }
                ]
            },
            {
                roman: 'II',
                title: 'Công Tác Cơ Cấu Tổ Chức và Nhân Sự',
                childrents: [
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    }
                ]
            }
        ];
    }

    onFullScreen() {
        console.log('fullscreen');
        const el = document.getElementById('fullScreen');
        if (screenfull.enabled) {
            screenfull.request(el);
        }
    }

    onCollapseShow() {
        $('.arrCollapse').parents('tbody').find('tr:not(:first-child)').show();
    }

    onCollapseHide() {
        $('.arrCollapse').parents('tbody').find('tr:not(:first-child)').hide();
    }
    public doSomething(date: any): void {
        console.log('Picked date: ', date);
        this.modalRef.hide();
    }

    onCancelPopup() {
        this.modalRef.hide();
    }

    ngOnInit() {
        this.datas = [
            {
                roman: 'I',
                title: 'Công Tác Quản Trị Đại Học',
                childrents: [
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    }
                ]
            },
            {
                roman: 'II',
                title: 'Công Tác Cơ Cấu Tổ Chức và Nhân Sự',
                childrents: [
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    },
                    {
                        title: 'Rà soát hệ thống văn bản, quản trị hiện hành theo lĩnh vực hoạt động của đơn vị',
                        kpis: 'Danh mục các hệ thống văn bản, quản lý quản trị của đơn vị',
                        plans: [
                            {
                                title: 'Lập kế hoạch thực hiện',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổ Chức các hoạt động',
                                startTime: '09/2017',
                                endTime: '10/2017'
                            },
                            {
                                title: 'Tổng Kết, đánh giá, ban hành Danh mục hệ thống văn bản về quản lý - quản trị nhà trường',
                                startTime: '09/2017',
                                endTime: '10/2018'
                            }
                        ],
                        boss: 'PGS-TS. Đỗ Văn Dũng',
                        department: 'Trung Tâm CNPM',
                        deparment_im: 'Các Đơn Vị'
                    }
                ]
            }
        ];

        this.exampleData = [
            {
                id: '1',
                text: 'CÔNG TÁC QUẢN TRỊ ĐẠI HỌC'
            },
            {
                id: '2',
                text: 'CÔNG TÁC CƠ CẤU TỔ CHỨC VÀ NHÂN SỰ'
            },
            {
                id: '3',
                text: 'CÔNG TÁC ĐẢM BẢO CHẤT LƯỢNG'
            },
            {
                id: '4',
                text: 'CÔNG TÁC TÀI CHÍNH VÀ CƠ SỞ VẬT CHẤT'
            }
        ];

        this.keHoachs = [
            {
                name: 'Lập kế hoạch thực hiện',
                startDate: '09/2017',
                endDate: '10/2017'
            },
            {
                name: 'Tổ chức các hoạt động',
                startDate: '11/2017',
                endDate: '02/2018'
            },
            {
                name: 'Bản kế hoạch tổng thể để khắc phục các điểm còn tồn tại sau Kiểm định chất lượng nhà trường và 04 CTĐT theo AUN - QA được phê duyệt',
                startDate: '03/2018',
                endDate: '03/2018'
            }
        ];
    }

    ngAfterViewInit() {
        $('.select-comb').select2({ width: '100%' });

        $('.check-cols-u .check-cols input').click(function (e) {
            e.stopPropagation();
        });
    }

    getIndex(id: string) {

        // animation collapse js
        $(`.index_${id}`).parents('tbody').find(`tr.row_${id}`).toggle();

    }
}
