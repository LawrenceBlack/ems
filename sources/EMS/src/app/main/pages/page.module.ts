import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageRoutingModule } from './page-routing.module';
import { PageComponent } from './page.component';
import { SharedModule } from '../../core/modules/shared.module';

// components
import { PageDesCommentComponent } from './des-comment/des-comment.component';
import { PageDocumentRelatedComponent } from './document-related/document-related.component';
import { PageEvidenceCommentComponent } from './evidence-comment/evidence-comment.component';
import { PageNotificationComponent } from './notification/notification.component';

@NgModule({
    imports: [
        CommonModule,
        PageRoutingModule,
        SharedModule
    ],
    declarations: [
        PageComponent,
        PageDesCommentComponent,
        PageDocumentRelatedComponent,
        PageEvidenceCommentComponent,
        PageNotificationComponent
    ]
})
export class PageModule { }
