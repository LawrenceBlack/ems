import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'page-document-related',
    templateUrl: './document-related.component.html',
    styleUrls: ['./document-related.component.scss']
})
export class PageDocumentRelatedComponent implements OnInit {
    file: any;
    constructor() { }
    datas = [
        {
            imageUrl: 'https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/m4rF-x66NqB.png?_nc_eui2=AeF7k1LhdgM160yxcXzh_rSZAMBl7J2JyR2awDq3fwhTvGqXRXZ9Hxe6n9y5G-2dT3xWGVoqIMoBjAdNLWA0YtmlVlNgML0VH-g7wHX2DV-cEA',
            name: 'GiaiMaDiaChi.docx',
            icon: 'fa-file-word-o',
            type: 'DOCX',
            user: 'Trần Quốc Thiện',
            org: 'Trung Tâm CNPM',
            modified_date: '14 January 2016 at 21:45',
            size: '24 MB',
            view: 12313,
            download: 1313
        },
        {
            imageUrl: 'https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/m4rF-x66NqB.png?_nc_eui2=AeF7k1LhdgM160yxcXzh_rSZAMBl7J2JyR2awDq3fwhTvGqXRXZ9Hxe6n9y5G-2dT3xWGVoqIMoBjAdNLWA0YtmlVlNgML0VH-g7wHX2DV-cEA',
            name: 'GiaiMaDiaChi.pdf',
            icon: 'fa-file-pdf-o',
            type: 'PDF',
            user: 'Nguyễn Trọng Nghĩa',
            org: 'Phòng ĐBCL',
            modified_date: '14 January 2016 at 21:45',
            size: '24 MB',
            view: 12313,
            download: 1313
        },
        {
            imageUrl: 'https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/m4rF-x66NqB.png?_nc_eui2=AeF7k1LhdgM160yxcXzh_rSZAMBl7J2JyR2awDq3fwhTvGqXRXZ9Hxe6n9y5G-2dT3xWGVoqIMoBjAdNLWA0YtmlVlNgML0VH-g7wHX2DV-cEA',
            name: 'so_hoa_minh_chung.xlsx',
            icon: 'fa-file-excel-o',
            type: 'DOCX',
            user: 'Trần Quốc Thiện',
            org: 'Phòng Tổ Chức - Hành Chính',
            modified_date: '14 January 2016 at 21:45',
            size: '24 MB',
            view: 12313,
            download: 1313
        },
        {
            imageUrl: 'https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/m4rF-x66NqB.png?_nc_eui2=AeF7k1LhdgM160yxcXzh_rSZAMBl7J2JyR2awDq3fwhTvGqXRXZ9Hxe6n9y5G-2dT3xWGVoqIMoBjAdNLWA0YtmlVlNgML0VH-g7wHX2DV-cEA',
            name: 'GiaiMaDiaChi.docx',
            icon: 'fa-file-word-o',
            type: 'DOCX',
            user: 'Trần Quốc Thiện',
            org: 'Trung Tâm CNPM',
            modified_date: '14 January 2016 at 21:45',
            size: '24 MB',
            view: 12313,
            download: 1313
        },
        {
            imageUrl: 'https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/m4rF-x66NqB.png?_nc_eui2=AeF7k1LhdgM160yxcXzh_rSZAMBl7J2JyR2awDq3fwhTvGqXRXZ9Hxe6n9y5G-2dT3xWGVoqIMoBjAdNLWA0YtmlVlNgML0VH-g7wHX2DV-cEA',
            name: 'GiaiMaDiaChi.pdf',
            icon: 'fa-file-pdf-o',
            type: 'PDF',
            user: 'Nguyễn Trọng Nghĩa',
            org: 'Phòng ĐBCL',
            modified_date: '14 January 2016 at 21:45',
            size: '24 MB',
            view: 12313,
            download: 1313
        },
        {
            imageUrl: 'https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/m4rF-x66NqB.png?_nc_eui2=AeF7k1LhdgM160yxcXzh_rSZAMBl7J2JyR2awDq3fwhTvGqXRXZ9Hxe6n9y5G-2dT3xWGVoqIMoBjAdNLWA0YtmlVlNgML0VH-g7wHX2DV-cEA',
            name: 'so_hoa_minh_chung.xlsx',
            icon: 'fa-file-excel-o',
            type: 'DOCX',
            user: 'Trần Quốc Thiện',
            org: 'Phòng Tổ Chức - Hành Chính',
            modified_date: '14 January 2016 at 21:45',
            size: '24 MB',
            view: 12313,
            download: 1313
        },
        {
            imageUrl: 'https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/m4rF-x66NqB.png?_nc_eui2=AeF7k1LhdgM160yxcXzh_rSZAMBl7J2JyR2awDq3fwhTvGqXRXZ9Hxe6n9y5G-2dT3xWGVoqIMoBjAdNLWA0YtmlVlNgML0VH-g7wHX2DV-cEA',
            name: 'GiaiMaDiaChi.docx',
            icon: 'fa-file-word-o',
            type: 'DOCX',
            user: 'Trần Quốc Thiện',
            org: 'Trung Tâm CNPM',
            modified_date: '14 January 2016 at 21:45',
            size: '24 MB',
            view: 12313,
            download: 1313
        },
        {
            imageUrl: 'https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/m4rF-x66NqB.png?_nc_eui2=AeF7k1LhdgM160yxcXzh_rSZAMBl7J2JyR2awDq3fwhTvGqXRXZ9Hxe6n9y5G-2dT3xWGVoqIMoBjAdNLWA0YtmlVlNgML0VH-g7wHX2DV-cEA',
            name: 'GiaiMaDiaChi.pdf',
            icon: 'fa-file-pdf-o',
            type: 'PDF',
            user: 'Nguyễn Trọng Nghĩa',
            org: 'Phòng ĐBCL',
            modified_date: '14 January 2016 at 21:45',
            size: '14 MB',
            view: 12313,
            download: 1313
        },
        {
            imageUrl: 'https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/m4rF-x66NqB.png?_nc_eui2=AeF7k1LhdgM160yxcXzh_rSZAMBl7J2JyR2awDq3fwhTvGqXRXZ9Hxe6n9y5G-2dT3xWGVoqIMoBjAdNLWA0YtmlVlNgML0VH-g7wHX2DV-cEA',
            name: 'so_hoa_minh_chung.xlsx',
            icon: 'fa-file-excel-o',
            type: 'DOCX',
            user: 'Trần Quốc Thiện',
            org: 'Phòng Tổ Chức - Hành Chính',
            modified_date: '14 January 2016 at 21:45',
            size: '90 MB',
            view: 1231332,
            download: 113
        }
    ];

    showInfoFile(item: any[]) {
        this.file = item;
    }
    seeMore() {
        let data = {
            imageUrl: 'https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/m4rF-x66NqB.png?_nc_eui2=AeF7k1LhdgM160yxcXzh_rSZAMBl7J2JyR2awDq3fwhTvGqXRXZ9Hxe6n9y5G-2dT3xWGVoqIMoBjAdNLWA0YtmlVlNgML0VH-g7wHX2DV-cEA',
            name: 'so_hoa_minh_chung.xlsx',
            icon: 'fa-file-excel-o',
            type: 'DOCX',
            user: 'Trần Quốc Thiện',
            org: 'Phòng Tổ Chức - Hành Chính',
            modified_date: '14 January 2016 at 21:45',
            size: '90 MB',
            view: 1231332,
            download: 113
        };
        this.datas.push(data);
    }

    ngOnInit() {
        this.file = this.datas[0];
    }
}
