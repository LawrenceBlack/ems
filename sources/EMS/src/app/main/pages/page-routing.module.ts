import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageComponent } from './page.component';

// components
import { PageDesCommentComponent } from './des-comment/des-comment.component';
import { PageDocumentRelatedComponent } from './document-related/document-related.component';
import { PageEvidenceCommentComponent } from './evidence-comment/evidence-comment.component';
import { PageNotificationComponent } from './notification/notification.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'page',
        pathMatch: 'full'
    },
    {
        path: '',
        component: PageComponent,
        children: [
            {
                path: 'page',
                redirectTo: 'overview',
                pathMatch: 'full'
            },
            {
                path: 'notification',
                component: PageNotificationComponent
            },
            {
                path: 'document',
                component: PageDocumentRelatedComponent
            },
            {
                path: 'evidence',
                component: PageEvidenceCommentComponent
            },
            {
                path: 'des',
                component: PageDesCommentComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PageRoutingModule { }
