import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'page-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss']
})
export class PageNotificationComponent implements OnInit {

    danhSachThongBaos: any[];
    constructor() { }

    readMore() {
        alert('read more !');
    }

    ngOnInit() {
        this.danhSachThongBaos = [
			{
				imageUrl: '../../../../assets/img/faces/face-6.jpg',
				userName: 'Trần Quốc Thiện',
				action: 'Upload 1 file minh chứng lên hệ thống'
			}, {
				imageUrl: '../../../../assets/img/faces/face-5.jpg',
				userName: 'Đoàn Văn Thanh Phong',
				action: 'Upload 1 video 4gb lên hệ thống'
			}, {
				imageUrl: '../../../../assets/img/faces/face-6.jpg',
				userName: 'Hồ Đặng Hữu Trọng',
				action: 'Vừa comment vào nội dung bạn vừa tải lên'
			}, {
				imageUrl: '../../../../assets/img/faces/face-4.jpg',
				userName: 'Trần Quốc Thiện',
				action: 'Upload 1 file minh chứng lên hệ thống'
			}, {
				imageUrl: '../../../../assets/img/faces/face-3.jpg',
				userName: 'Trần Quốc Thiện',
				action: 'Upload 1 file minh chứng lên hệ thống'
			}, {
				imageUrl: '../../../../assets/img/faces/face-3.jpg',
				userName: 'Trần Quốc Thiện',
				action: 'Upload 1 file minh chứng lên hệ thống'
			}, {
				imageUrl: '../../../../assets/img/faces/face-2.jpg',
				userName: 'Trần Quốc Thiện',
				action: 'Upload 1 file minh chứng lên hệ thống'
			}, {
				imageUrl: '../../../../assets/img/faces/face-1.jpg',
				userName: 'Trần Quốc Thiện',
				action: 'Upload 1 file minh chứng lên hệ thống'
			}, {
				imageUrl: '../../../../assets/img/faces/face-6.jpg',
				userName: 'Trần Quốc Thiện',
				action: 'Upload 1 file minh chứng lên hệ thống'
			}, {
				imageUrl: '../../../../assets/img/faces/face-4.jpg',
				userName: 'Trần Quốc Thiện',
				action: 'Upload 1 file minh chứng lên hệ thống'
			}
		];
    }
}
