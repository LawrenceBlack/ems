import { Component } from '@angular/core';
import { AuthenticationService } from './core/services/authentication.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	title = 'app';
	constructor(private router: Router, private authenticationService: AuthenticationService) {
		// check logged
		const isAuth = this.authenticationService.isAuthenticated();
		if (isAuth) {
			//check role
			const role = this.authenticationService.checkUtilRole();
			// if (role === 0) {
			// 	console.log('roles');
			// 	this.router.navigate(['roles']);
			// }
			if (role === 1) {
				console.log('client');
				this.router.navigate(['main/client']);
			}
			if (role === 2) {
				console.log('department');
				this.router.navigate(['main/department']);
			}
			if (role === 3) {
				console.log('ems');
				this.router.navigate(['main/ems']);
			}

		} else {
			this.router.navigate(['login']);
		}
	}
}
