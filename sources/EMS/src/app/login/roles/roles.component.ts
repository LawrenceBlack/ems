import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UrlConstant } from '../../core/commons/url.constant';
import { MessageConstant } from '../../core/commons/message.constant';
import { RolesService } from '../../core/services/roles/roles.service';
import { AuthenticationService } from '../../core/services/authentication.service';
import { ShareService } from '../../core/services/share.service';
import { UtilService } from '../../core/services/util.service';
import { SystemConstant } from '../../core/commons/system.constant';
import { NotifiCationService } from '../../core/services/notification.service';
import { SocialUser } from 'angular4-social-login';


@Component({
    selector: 'app-roles',
    templateUrl: './roles.component.html',
    styleUrls: ['./roles.component.scss'],
    providers: [RolesService, NotifiCationService, UtilService]
})

export class RolesComponent implements OnInit {

    hostName: String;
    baseUrl: String;
    unitsId: Array<any> = [];
    id: String = '0';
    message: String;
    constructor(
        private router: Router,
        private rolesService: RolesService,
        private shareService: ShareService,
        private utilService: UtilService,
        private notifycationService: NotifiCationService,
        private authenticationService: AuthenticationService) {
        // check
        this.hostName = window.location.host;
        this.baseUrl = this.hostName === UrlConstant.hostName ? '../..' : '.';
        const checkTokenGoogle: boolean = this.authenticationService.getTokenGoogleFromLocal();
        if (!checkTokenGoogle) {
            this.router.navigate(['/login']);
        }
        // else {
        //     // get role of user from server by email google
        //     this.authenticationService.getSocialUserGoogle().first().subscribe((user) => {

        //     })

        // }
    }

    ngOnInit() {
        this.checkFullPageBackgroundImage();
        this.getAllUnitsRole();
        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 300);
    }

    checkFullPageBackgroundImage() {
        let $page = $('.full-page');
        let image_src = $page.data('image');
        if (image_src !== undefined) {
            let image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>';
            $page.append(image_container);
        }
    }

    getAllUnitsRole() {
        this.rolesService.getAllRoles().subscribe(
            (data) => {
                this.unitsId = data;
            }, (err) => {
                console.log(err);
                this.unitsId = [];
            }
        );
    }

    goToMain() {
        if (this.id === '0') {
            this.message = MessageConstant.MSG_CHOOSE_UNIT;
            return;
        } else {
            // get token step 1
            const tokenStep1: string = this.shareService.getTokenStep_1();
            this.authenticationService.getTokenStep_2(tokenStep1, this.id)
                .subscribe(
                    (data) => {
                        // get token final
                        this.utilService.setLocalStorage(SystemConstant.TOKEN_KEY, data['token']);
                        const roles: Array<string> = data['roles'];
                        // check unit role
                        // encrypt role
                        let role: string = '';
                        if (roles.indexOf(SystemConstant.SYSTEM_ROLE_USER) !== -1) {
                            // set thông báo
                            this.notifycationService.alertSuccess(MessageConstant.MSG_LOGIN_GOOGLE_SUCCESS);
                            role = this.utilService.encryptKey(SystemConstant.SYSTEM_ROLE_USER, SystemConstant.SECRECT_KEY);
                            this.router.navigate(['/main/client']);
                        } else {
                            if (roles.indexOf(SystemConstant.SYSTEM_ROLE_ADMIN) !== -1) {
                                this.notifycationService.alertSuccess(MessageConstant.MSG_LOGIN_GOOGLE_SUCCESS);
                                role = this.utilService.encryptKey(SystemConstant.SYSTEM_ROLE_ADMIN, SystemConstant.SECRECT_KEY);
                                this.router.navigate(['/main/department']);
                            } else {
                                if (roles.indexOf(SystemConstant.SYSTEM_ROLE_SUPER_ADMIN) !== -1) {
                                    this.notifycationService.alertSuccess(MessageConstant.MSG_LOGIN_GOOGLE_SUCCESS);
                                    role = this.utilService.encryptKey(SystemConstant.SYSTEM_ROLE_SUPER_ADMIN, SystemConstant.SECRECT_KEY);
                                    this.router.navigate(['/main/ems']);
                                } else {
                                    this.notifycationService.alertError(MessageConstant.MSG_ERROR_UNIT_ROLE);
                                    return;
                                }
                            }
                        }
                        // save role to localstorage
                        this.utilService.setLocalStorage(SystemConstant.ROLE_KEY, role);
                    }, (err) => {
                        this.notifycationService.alertError(MessageConstant.MSG_ERROR_ACCESS);
                        return;
                    }
                );
        }
    }
}
