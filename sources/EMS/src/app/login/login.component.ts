import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UrlConstant } from '../core/commons/url.constant';
import { MessageConstant } from '../core/commons/message.constant';
import { NotifiCationService } from '../core/services/notification.service';
import { AuthService, SocialUser, GoogleLoginProvider } from 'angular4-social-login';
import { AuthenticationService } from '../core/services/authentication.service';
import { ShareService } from '../core/services/share.service';
import { UtilService } from '../core/services/util.service';
import { SystemConstant } from '../core/commons/system.constant';

/* declare interface User {
	email?: string;
	password?: string;
}
*/

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
	providers: [NotifiCationService]
})
export class LoginComponent implements OnInit, AfterViewInit {
	// public login: User;
	hostName: string;
	baseUrl: string;
	socialUser: SocialUser;

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private authGoogleService: AuthService,
		private notifyCationService: NotifiCationService,
		private authenticationService: AuthenticationService,
		private shareService: ShareService,
		private utilService: UtilService
	) {

		this.authenticationService.logout();
		this.hostName = window.location.host;
		this.baseUrl = this.hostName === UrlConstant.hostName ? '../..' : '.';

	}

	checkFullPageBackgroundImage() {
		let $page = $('.full-page');
		let image_src = $page.data('image');
		if (image_src !== undefined) {
			let image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>';
			$page.append(image_container);
		}
	}

	/* 	onLogin(model: User, isValid: boolean) {
			if (isValid) {
				console.log(model, isValid);
				this.router.navigate(['/management/dashboard']);
			}
		}
	*/

	ngOnInit() {
		/* 		this.login = {
					email: '',
					password: ''
				};
		*/

		this.checkFullPageBackgroundImage();

		setTimeout(function () {
			// after 1000 ms we add the class animated to the login/register card
			$('.card').removeClass('card-hidden');
		}, 700);
	}

	ngAfterViewInit() { }

	signInWithGoogle(): void {
		this.authenticationService.signInGoogle()
			.then((user: SocialUser) => {
				// gửi thông tin xác thực google lên server
				this.authenticationService.verifyToken(user['authToken']).subscribe(
					(data) => {
						this.shareService.setListRoleUnitsId(data['unitsId']);
						this.shareService.setTokenStep_1(data['tokenVerify']);
						// this.shareService.setTokenStepGoogle(user.authToken);
						this.utilService.setLocalStorage(SystemConstant.TOKEN_GOOGLE, data['tokenVerify']);
						// this.utilService.setLocalStorage(SystemConstant.LIST_UNITS_ROLE, data['unitsId']);
						this.router.navigate(['/login/roles']);
					},
					(err) => {
						this.notifyCationService.alertError(MessageConstant.MSG_ERROR_LOGIN_SERVER);
						// đăng xuất google
						this.authenticationService.signOutWithGoogle();
						return;
					}
				);
			})
			.catch(() => {
				this.notifyCationService.alertError(MessageConstant.MSG_ERROR_LOGIN_GOOGLE);
			});
	}
}
