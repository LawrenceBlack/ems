import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import { RolesComponent } from './roles/roles.component';

const routes: Routes = [

    {
        path: '',
        component: LoginComponent,
    },
    {
        path: 'roles',
        component: RolesComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LoginRoutingModule { }
