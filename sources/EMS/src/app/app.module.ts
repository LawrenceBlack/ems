import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider } from 'angular4-social-login';
import { AppComponent } from './app.component';
import { SystemConstant } from './core/commons/system.constant';

// share service
import { ShareService } from './core/services/share.service';
import { UtilService } from './core/services/util.service';
import { AuthGuard } from './core/services/authguard/auth.guard';
import { AuthenticationService } from './core/services/authentication.service';



const config = new AuthServiceConfig([
	{
		id: GoogleLoginProvider.PROVIDER_ID,
		provider: new GoogleLoginProvider(SystemConstant.CLIENT_ID_GOOGLE)
	}
]);

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		CommonModule,
		HttpClientModule,
		AppRoutingModule,
		SocialLoginModule.initialize(config)
	],
	providers: [
		ShareService, UtilService, AuthGuard, AuthenticationService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
